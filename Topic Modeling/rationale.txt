This project was done as a downstream task that uses Topic Modeling to provide
topics/themes about the Office of the Vice President Budget as `classes` for the
machine text classification. The following text is simply a summary of the task
and does not encompass everything done in the code.

1) The Philippine's Office of the Vice President (OVP) and the VP herself
   was a controversial topic due to the entities' demeanor towards the
   senate and how they questionably use or spend their budget.

2) That being said: countless news articles from journalistic websites, news outlets,
   and from the website of the OVP itself; were scraped through "pyppeteer", selenium, 
   and beautiful soup. Information from Facebook pages, comments, reddit posts, 
   and twitter data were all gathered through their respective APIs, pyppeteer, 
   GitHub libraries, and chrome extensions. All data that was gathered is related to the
   office of the vice president and concerns about their budget. This collected data
   contains 3900 rows of text data.

3) The collected text data was compiled into one column as a csv file. Preprocessing techniques
   were utilized to handle inconsistencies and removal of unnecessary words. Sequences of Tagalog
   phrases or text are taken and translated as a whole instead of doing it word per word. The tool
   used for this was a transformers model named "No Language Left Behind" by Meta. The architecture
   of transformers is useful for translating the sequence of Tagalog texts because of its attention
   layers. Other preprocessing techniques were applied.

4) LSA was used as the topic modeling technique, which uses Singular Value Decomposition to extract
   the themes from the documents. The themes with the most optimal `k` number of text were analyzed
   through various metrics for verification. These themes were then used as classes that were assigned
   to each document. 

   Themes extracted:

   a) Budgetary control and hearings from Commission on Audit
   b) References to funding and discretionary spending decisions of the OVP
   c) Confidential Funds and the budget/monetary requests of Vice President Sara Duterte
   d) Relief efforts of the Vice President
   e) VP Sara Duterte's controversial book "Isang Kaibigan" 

5) The labeled data with their corresponding classes have been analyzed and verified. Class imbalance
   is addressed through data augmentation of the training set. Logistic Regression was the model used
   to do text classification. GridSearchCV was used for hyperparameter tuning and acquire the most optimal
   model.
   