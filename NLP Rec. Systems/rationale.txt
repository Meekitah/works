Recommendation tasks usually implore users, products, and explicit ratings as features.
However, we may be missing out on pertinent information from user reviews. This project
delves deep into incorporating an NLP task to process user reviews and look for corresponding
product descriptions that match user needs. These additional features, along with the typical
Recommendation System features are applied in two models: 

1) Singular Vector Decomposition (SVD) with NLP
2) Artificial Neural Network (ANN) with NLP

Additionally two models that do not implore the NLP task for processing the additional features
are made for comparison:

1) Baseline SVD
2) Baseline ANN 

Overall, the project covers these objectives:

a. Determine the effectiveness of integrating text data and natural language processing techniques
   in the recommender systems’ performance metrics.


b. Present a natural language processing pipeline to process text data that can be utilized by the
   recommender system models.


c. Compare the performance of the model that integrates natural language processing techniques to
   the baseline models that did not include natural language processing techniques.
