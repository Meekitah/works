const apiKey = "RGAPI-c707216e-c6be-426e-be45-af61b727b8d5"; //changes every 24 hours btw

window.onload = function() {
    getValoLb(currentEpIndex + 1, currentReIndex + 1);
  };

document.getElementById('toggleList').addEventListener('click', function() {
    console.log("test");
    const episodeList = document.getElementById('episodeTable');
    episodeList.style.display = (episodeList.style.display === 'block') ? 'none' : 'block';
});

document.getElementById('regionList').addEventListener('click', function() {
    const episodeList = document.getElementById('regionTable');
    episodeList.style.display = (episodeList.style.display === 'block') ? 'none' : 'block';
});

document.addEventListener("DOMContentLoaded", function () {
    const episodeLinks = document.querySelectorAll("#regionsList a");
    const toggleListHeading = document.querySelector("#regionList h1");
  
    episodeLinks.forEach(function (link) {
      link.addEventListener("click", function (event) {
        event.preventDefault();
  
        const newHeadingText = link.textContent;
        toggleListHeading.textContent = newHeadingText;
        const episodeList = document.getElementById('regionTable');
        episodeList.style.display = (episodeList.style.display === 'block') ? 'none' : 'block';
      });
    });
  });

document.addEventListener("DOMContentLoaded", function () {
    const episodeLinks = document.querySelectorAll("#episodeList a");
    const toggleListHeading = document.querySelector("#toggleList h1");
  
    episodeLinks.forEach(function (link) {
      link.addEventListener("click", function (event) {
        event.preventDefault();
  
        const newHeadingText = link.textContent;
        toggleListHeading.textContent = newHeadingText;
        const episodeList = document.getElementById('episodeTable');
        episodeList.style.display = (episodeList.style.display === 'block') ? 'none' : 'block';
      });
    });
  });


/**
 * Author: Samson, Mik Iris G.
 * A leaderboard in the game Valorant is separated by Episodes and Acts.
 * The players differ each episode and act based on the region
 * This method displays the top 10 Valorant players in a specific Episode and Act + Region
 * @param episodeAct is the episode and act filter
 * @param region is the specific region filter
 */
async function getValoLb(episodeAct, region) {
    console.log(episodeAct,region)

    try{
        const epAct = chooseEpisodeAct(episodeAct);
        const reg = chooseValoRegion(region);
        console.log(reg)
        const request = await valoCompeRequestBuilder(epAct, reg);
        console.log(request)
        const response = await fetch(request + apiKey);
        const data = await response.json();
        const players = data.players;
        const tableBody = document.getElementById("body");
        while (tableBody.firstChild) {
            tableBody.removeChild(tableBody.firstChild);
        }
        players.forEach((element, index) => {

            const row = document.createElement("tr");

            const rankColumn = document.createElement("td");
            rankColumn.id = "rank"
            rankColumn.innerHTML = `<h1>${index + 1}</h1>`;
            
            const ratingColumn = document.createElement("td");
            ratingColumn.className = "info";
            ratingColumn.id = "rating";
            ratingColumn.innerHTML = `<h4>${element.rankedRating}</h4>`;
            
            const nameColumn = document.createElement("td");
            nameColumn.className = "info";
            nameColumn.id = "name";
            nameColumn.innerHTML = `<h1>${element.gameName}#${element.tagLine}</h1>`;

            const winsColumn = document.createElement("td");
            winsColumn.id = "rank"
            winsColumn.innerHTML = `<h3>${element.numberOfWins}</h3>`;
        
            // Append the columns to the row
            row.appendChild(rankColumn);
            row.appendChild(ratingColumn);
            row.appendChild(nameColumn);
            row.appendChild(winsColumn);
        
            // Append the row to the table body
            tableBody.appendChild(row);
        });
    }
    catch(error){
        console.error("There was a problem with fetching the necessary data; our most sincere apologies.", error);
    }
} // end of getValoLb()

/**
 * Author: Samson, Mik Iris G.
 * The API request for getting the top 10 valorant players filtered by episode and act + region
 * @param epAct is the episode and act filter
 * @param region is the specific region filter
 */
function valoCompeRequestBuilder(epact, region) {
    return "https://" + region + ".api.riotgames.com/val/ranked/v1/leaderboards/by-act/" + epact + "?size=" + 10 + "&startIndex=0&api_key=";
}

/**
 * Author: Samson, Mik Iris G.
 * Switch case is used in the drop down event for choosing a region
 * Essentially filters the Valorant player leaderboard by a specific region
 * @param region is the specific region filter
 */
function chooseValoRegion(region){
    let regionChoice;
    switch(region){
        case 1:
            regionChoice = "ap";
            break;
        case 2:
            regionChoice = "na";
            break;
        case 3:
            regionChoice = "kr";
            break;
        case 4:
            regionChoice = "eu";
            break;
        case 5:
            regionChoice = "br";
            break;
        case 6:
            regionChoice = "latam";
            break;
        default:
            break;               
    }
    return regionChoice;
}

/**
 * Author: Samson, Mik Iris G.
 * Switch case is used in the drop down event for choosing the specific episode and act
 * Each epsode and act has a specific id. Method maps the choice from the drop down bar to the id.
 * Essentially filters the Valorant player leaderboard by a chosen episode and act
 * @param choice is the episode and act filter
 */
function chooseEpisodeAct(choice) {
    let epActChoice;

    switch(choice){
        case 1:
            epActChoice = "97b6e739-44cc-ffa7-49ad-398ba502ceb0";
            break;
        case 2:
            epActChoice = "ab57ef51-4e59-da91-cc8d-51a5a2b9b8ff";
            break;
        case 3:
            epActChoice = "52e9749a-429b-7060-99fe-4595426a0cf7";
            break;
        case 4:
            epActChoice = "2a27e5d2-4d30-c9e2-b15a-93b8909a442c";
            break;
        case 5:
            epActChoice = "4cb622e1-4244-6da3-7276-8daaf1c01be2";
            break;
        case 6:
            epActChoice = "a16955a5-4ad0-f761-5e9e-389df1c892fb";
            break;
        case 7:
            epActChoice = "573f53ac-41a5-3a7d-d9ce-d6a6298e5704";
            break;
        case 8:
            epActChoice = "d929bc38-4ab6-7da4-94f0-ee84f8ac141e";
            break;
        case 9:
            epActChoice = "3e47230a-463c-a301-eb7d-67bb60357d4f";
            break;
        case 10:
            epActChoice = "67e373c7-48f7-b422-641b-079ace30b427";
            break;
        case 11:
            epActChoice = "7a85de9a-4032-61a9-61d8-f4aa2b4a84b6";
            break;
        case 12:
            epActChoice = "aca29595-40e4-01f5-3f35-b1b3d304c96e";
            break;
        case 13:
            epActChoice = "9c91a445-4f78-1baa-a3ea-8f8aadf4914d";
            break;
        case 14:
            epActChoice = "34093c29-4306-43de-452f-3f944bde22be";
            break;
        case 15:
            epActChoice = "2de5423b-4aad-02ad-8d9b-c0a931958861";
            break;
        case 16:
            epActChoice = "0981a882-4e7d-371a-70c4-c3b4f46c504a";
            break;
        case 17:
            epActChoice = "03dfd004-45d4-ebfd-ab0a-948ce780dac4";
            break;
        case 18:
            epActChoice = "4401f9fd-4170-2e4c-4bc3-f3b4d7d150d1";
            break;    
        default:
            break;
    }
    return epActChoice;
}

var currentEpIndex = 0;
var currentReIndex = 0;

const episodeList = document.getElementById("episodeList");
const episodeLinks = episodeList.querySelectorAll("li a");
episodeLinks.forEach((link, index) => {
    link.addEventListener("click", (event) => {
        event.preventDefault(); 
        currentEpIndex = index;
        getValoLb(currentEpIndex+1,currentReIndex+1)
    });
});


const regionList = document.getElementById("regionsList");
const regionLinks = regionList.querySelectorAll("li a");
regionLinks.forEach((link, index) => {
    link.addEventListener("click", (event) => {
        event.preventDefault(); 
        currentReIndex = index;
        getValoLb(currentEpIndex+1,currentReIndex+1)
    });
});