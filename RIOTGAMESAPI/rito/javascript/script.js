document.addEventListener("DOMContentLoaded", function() {
  const volumeButton = document.getElementById("volume-button");
  const backgroundVideo = document.getElementById("backgroundVideo");
  const backgroundImage = document.getElementById("backgroundImage")
  let isMuted = true; 
  

  volumeButton.addEventListener("click", function(event) {
    event.preventDefault(); 

    isMuted = !isMuted;
    const iconClass = isMuted ? "fas fa-volume-mute" : "fas fa-volume-up";
    volumeButton.innerHTML = '<i class="' + iconClass + '"></i>';
    backgroundVideo.muted = isMuted;
  
  });
  
  const btnContainer = document.querySelector(".bbb");
  const buttons = btnContainer.querySelectorAll(".btn");

  buttons.forEach((button) => {
    button.addEventListener("click", handleButtonClick);
    
  });

  let activeButton = buttons[0];
  const btnSlide = buttons[0].querySelector('.btn__inner')

  btnSlide.style.backgroundColor = "var(--highlight-color)"
  buttons[0].style.color = "#ece8e1" 
  

 function handleButtonClick(event) {
  const videoSource = event.currentTarget.dataset.video;
  // const imageSourse = event.currentTarget.dataset.Image;
  backgroundVideo.src = videoSource;
  // backgroundImage.src = imageSourse;

  
    const btnSlide = event.currentTarget.querySelector('.btn__inner')
    btnSlide.style.backgroundColor = "var(--highlight-color)"
    event.currentTarget.style.color = "#ece8e1" 

    let prevButton = activeButton
    console.log(activeButton !=  event.currentTarget)
    if (activeButton !=  event.currentTarget) {
      prevButton.style.color = "var(--highlight-color)"
      prevButton.querySelector('.btn__inner').style.backgroundColor = "";
    }
     activeButton = event.currentTarget;
   
  }

});




//WORKING
const buttonData = [
  { year: "2018", video: "/video/rise.mp4", Image: "/bg/rise.jpg" },
  { year: "2019", video: "/video/phoenix.mp4", Image: "/bg/phoenix.jpg" },
  { year: "2020", video: "/video/take over.mp4", Image: "/bg/take_over.jpg" },
  { year: "2021", video: "/video/burn it down.mp4", Image: "/bg/burn_it_down.jpg" },
  { year: "2022", video: "/video/star walking.mp4", Image: "/bg/star_walking.jpg" },
  { year: "2023", video: "/video/gods.mp4", Image: "/bg/gods.jpg" },
];


const btnContainer = document.querySelector(".bbb");

buttonData.forEach((data) => {
  const button = document.createElement("button");
  button.classList.add("btn", "btn--light");
  button.dataset.video = data.video;
  button.dataset.Image = data.Image;

  const btnInner = document.createElement("span");
  btnInner.classList.add("btn__inner");

  const btnSlide = document.createElement("span");
  btnSlide.classList.add("btn__slide");

  const btnContent = document.createElement("span");
  btnContent.classList.add("btn__content");
  btnContent.textContent = data.year;

  btnInner.appendChild(btnSlide);
  btnInner.appendChild(btnContent);
  button.appendChild(btnInner);
  btnContainer.appendChild(button);
});