const apiKey = "RGAPI-c66cd025-47c5-48b7-9c62-bedd4d258644"; //changes every 24 hours btw
var regionSelected;

//Modified: Asuncion, Jonathan
window.onload = function() {
    getTopTenLeague("Philippines","Solo/Duo","Bronze" );    
    generateOptions();
  };

  /**
   * Author: Jonathan Asuncion
   * calls getSummonerMatchDetails when search button is pressed.
   */
const search = document.getElementById("searchButton");
search.addEventListener("click", () => {
    const searchKey = document.getElementById("searchKey").value;
    getSummonerMatchDetails(searchKey, apiKey, 20, regionSelected);
});

/**
 * Author: Jonathan Asuncion
 * Saves the selected region in a global variable for later use
 */
const regSelect = document.getElementById("regList");
regSelect.onchange = () => {
    var selected = regSelect.selectedIndex;
    regionSelected = regSelect.options[selected].value;
    regionSelected = chooseRegion(regionSelected);
    console.log(regionSelected);
};

/**
 * Author: Jonathan Asuncion
 * generateOptions for region drop-down list for individual search
 */
function generateOptions() {
    var selectElement = document.getElementById("regList");

    var options = ["Philippines", "North America", "Korea", "EU West", "Japan", "Brazil", "EU North"];

    options.forEach(function(optionValue) {
        var option = document.createElement("option");
        option.value = optionValue; 
        option.text = optionValue; 
        selectElement.appendChild(option);
    });
    regionSelected = chooseRegion(options[0]);
}

/**
 * Author: Jonathan Asuncion
 * Listener for clicks for the game mode drop-down list.
 * It also calls the topTenLeague upon click.
 */
const gameList = document.getElementById("gameModes");
const gameLinks = gameList.querySelectorAll("li a");
gameLinks.forEach((link) => {
    link.addEventListener("click", (event) => {
        event.preventDefault()
        const reg = document.getElementById("region").innerText;
        const rank = document.getElementById("rankName").innerText;
        const gm = link.innerText;
        getTopTenLeague(reg,gm,rank );
    });
});

/**
 * Author: Jonathan Asuncion
 * Listener for clicks for the region drop-down list.
 * It also calls the topTenLeague upon click.
 */
const regionList = document.getElementById("regions");
const regionLinks = regionList.querySelectorAll("li a");
regionLinks.forEach((link) => {
    link.addEventListener("click", (event) => {
        event.preventDefault()
        const reg = link.innerText;
        const rank = document.getElementById("rankName").innerText;
        const gm = document.getElementById("mode").innerText;
        getTopTenLeague(reg,gm,rank );
    });
});

/**
 * Author: Jonathan Asuncion
 * Listener for clicks for the rank drop-down list.
 * It also calls the topTenLeague upon click.
 */
const rankList = document.getElementById("ranks");
const rankLinks = rankList.querySelectorAll("li a");
rankLinks.forEach((link) => {
    link.addEventListener("click", (event) => { 
        event.preventDefault()
        const reg = document.getElementById("region").innerText;
        const rank = link.innerText;
        const gm = document.getElementById("mode").innerText;
        getTopTenLeague(reg,gm,rank );
    });
});


/**
 * Author: Jonathan Asuncion
 * Listener for click on the game mode.
 * It hides/unhides the game list for selection.
 */
document.getElementById("gameList").addEventListener('click', function() {
    console.log("test1");
    const episodeList = document.getElementById("gameMode");
    episodeList.style.display = (episodeList.style.display === 'block') ? 'none' : 'block';
});

/**
 * Author: Jonathan Asuncion
 * Listener for click on the game mode.
 * It hides/unhides the game list for selection.
 */
document.getElementById('regionList').addEventListener('click', function() {
    console.log("test2");
    const episodeList = document.getElementById('regionTable');
    episodeList.style.display = (episodeList.style.display === 'block') ? 'none' : 'block';
});

/**
 * Author: Jonathan Asuncion
 * Listener for click on the game mode.
 * It hides/unhides the game list for selection.
 */
document.getElementById('rankList').addEventListener('click', function() {
    console.log("test3");
    const episodeList = document.getElementById('rankTable');
    episodeList.style.display = (episodeList.style.display === 'block') ? 'none' : 'block';
});

/**
 * Author: Jonathan Asuncion
 * Listener for click on the game mode.
 * It hides/unhides the game list for selection.
 */
document.addEventListener("DOMContentLoaded", function () {
    const regions = document.querySelectorAll("#regions a");
    const regionListHeading = document.querySelector("#regionList h1");
    console.log("test4")
    regions.forEach(function (link) {
      link.addEventListener("click", function (event) {
        event.preventDefault();
  
        const newHeadingText = link.textContent;
        regionListHeading.textContent = newHeadingText;
        const regionTable = document.getElementById('regionTable');
        regionTable.style.display = (regionTable.style.display === 'block') ? 'none' : 'block';
      });
    });
  });

/**
 * Author: Jonathan Asuncion
 * Listener for click on the rank title.
 * It hides/unhides the rank list for selection.
 */
document.addEventListener("DOMContentLoaded", function () {
    const episodeLinks = document.querySelectorAll("#ranks a");
    const toggleListHeading = document.querySelector("#rankList h1");
  
    episodeLinks.forEach(function (link) {
      link.addEventListener("click", function (event) {
        event.preventDefault();
  
        const newHeadingText = link.textContent;
        toggleListHeading.textContent = newHeadingText;
        const episodeList = document.getElementById('rankTable');
        episodeList.style.display = (episodeList.style.display === 'block') ? 'none' : 'block';
      });
    });
  });

  /**
 * Author: Jonathan Asuncion
 * Listener for click on the region.
 * It hides/unhides the region list for selection.
 */
  document.addEventListener("DOMContentLoaded", function () {
    const episodeLinks = document.querySelectorAll("#gameModes a");
    const toggleListHeading = document.querySelector("#gameList h1");
  
    episodeLinks.forEach(function (link) {
      link.addEventListener("click", function (event) {
        event.preventDefault();
  
        const newHeadingText = link.textContent;
        toggleListHeading.textContent = newHeadingText;
        const episodeList = document.getElementById('gameMode');
        episodeList.style.display = (episodeList.style.display === 'block') ? 'none' : 'block';
      });
    });
  });

        /**
         * Author: Samson, Mik Iris
         * Modified: Asuncion, Jonathan
         * Uses RIOT API to fetch the top 10 League Players under a (specific region)
         * Currently working on a functionality that also filters based on flex queue once buttons are here
        **/ 
        async function getTopTenLeague(region, gameMode, rank) {
            try {
                const playerRank = chooseRankFilter(rank);
                const reg = chooseRegion(region);
                const gm = chooseGameMode(gameMode);
                const request = leagueLbRequest(reg, gm, playerRank);
                const responseC = await fetch(request);
                const dataC = await responseC.json();
                const challengers = dataC.slice(0, 10);
                challengers.sort(compareByLP);
                const tableBody = document.getElementById("body");

                const path = getRankImagePath(playerRank);
                document.getElementById("rankIMG").src = path;

                while (tableBody.firstChild) {
                    tableBody.removeChild(tableBody.firstChild);
                }


                var index = 0;
                for (const challenger of challengers){

                    const puuid = await getPUUIDBySummID(reg, challenger.summonerId);
                    const iconID = await getIconByPUUID(reg, puuid);

                    const challIcon = document.createElement("img");
                    challIcon.setAttribute("id",`challIcon${index}`);
                    challIcon.src = iconID;
                    challIcon.width = 100;
                    challIcon.height = 100;

                    const row = document.createElement("tr");

                    const h1 = document.createElement("h1");
                    h1.textContent = index + 1;
                    
                    const h2 = document.createElement("h2");
                    h2.textContent = `${challenger.leaguePoints}LP`;

                    const lp = document.createElement("td");
                    lp.id = "rating";
                    lp.appendChild(challIcon);
                    lp.appendChild(h1);
                    lp.appendChild(h2);
                    
                    const wins = document.createElement("td");
                    wins.className = "info";
                    wins.id = "wins";
                    wins.innerHTML = `<h4>${challenger.wins}</h4>`;

                    const nameColumn = document.createElement("td");
                    nameColumn.className = "info";
                    nameColumn.id = "name";
                    nameColumn.innerHTML = `<h1>${challenger.summonerName}</h1>`;
        
                    const losses = document.createElement("td");
                    losses.id = "losses";
                    losses.innerHTML = `<h3>${challenger.losses}</h3>`;
                
                    // Append the columns to the row
                    
                    row.appendChild(lp);
                    row.appendChild(nameColumn);
                    row.appendChild(wins);
                    row.appendChild(losses);
                    
                
                    // Append the row to the table body
                    tableBody.appendChild(row);
                    index++;
                }
            }
            catch(error) {
                console.error("There was a problem with fetching the necessary data; our most sincere apologies.", error);
            }
            
        } // end of getChallengers()

        /**
        * Author: Samson, Mik Iris
        * Chooses a targeted region for displaying that server's League player leaderboard
        * Acts as a region choice and used along with leagueLbRequest method
        **/
        function chooseRegion(region){
            let regionChoice;
            switch(region){
                case "Philippines":
                    regionChoice = "ph2";
                    break;
                case "North America":
                    regionChoice = "na1";
                    break;
                case "Korea":
                    regionChoice = "kr";
                    break;
                case "EU West":
                    regionChoice = "euw1";
                    break;
                case "Japan":
                    regionChoice = "jp";
                    break;
                case "Brazil":
                    regionChoice = "br1";
                    break;
                case "EU North":
                    regionChoice = "eun1";
                    break;
                default:
                    break;               
            }
            console.log(regionChoice);
            return regionChoice;
        }

        /**
         * Author: Samson, Mik Iris
         * Filters a rank choice for the League of Legends leaderboard and is utilized for the League API Builder
         * Used along with leagueLbRequest method's rank option
         * @param rank 
         * @returns rank choice of user
         */
        function chooseRankFilter(rank){
            let rankChoice;

            switch(rank){
                case "Iron":
                    rankChoice = "IRON";
                    break;
                case "Bronze":
                    rankChoice = "BRONZE";
                    break;
                case "Silver":
                    rankChoice = "SILVER";
                    break;
                case "Gold":
                    rankChoice = "GOLD";
                    break; 
                case "Platinum":
                    rankChoice = "PLATINUM";
                    break;
                case "Diamond":
                    rankChoice = "DIAMOND";
                    break;
                case "Master":
                    rankChoice = "MASTER";
                    break;
                case "Grandmaster":
                    rankChoice = "GRANDMASTER";
                    break;     
                case "Challenger":
                    rankChoice = "CHALLENGER";
                    break;
                case "Unranked":
                    rankChoice = "unranked";
                    break;                       
                default:
                    break;
            }
            
            console.log(rankChoice);
            return rankChoice;
        }

        /**
         * Author: Samson, Mik Iris
         * Specifies gamemode for displaying League leaderboard
         * Used along with leagueLbRequest method's game mode choice
        **/
        function chooseGameMode(gameMode){
            let gameModeChoice;

            switch (gameMode){
                case "Solo/Duo":
                    gameModeChoice = "RANKED_SOLO_5x5";
                    break;
                case "Flex":
                    gameModeChoice = "RANKED_FLEX_SR";
                    break;
                default:
                    break;
            }
            
            console.log(gameModeChoice);
            return gameModeChoice;
        }

        /**
         * Author: Samson, Mik Iris
        * Request API builder for the league leaderboard 
        **/
        function leagueLbRequest(region, gameMode, rank){
            return "https://" + region + ".api.riotgames.com/lol/league-exp/v4/entries/" + gameMode + "/" + rank + "/I?page=1&api_key=" + apiKey;
        }

        /** 
         * Author: Samson, Mik Iris
         * Searches a summoner through summoner name/account name
         * ssbn = Search Summoner By Name
         * Further implementation = search by region once buttons are implemented
        **/ 
        function ssbnRequestBuilder(nameToSearch) {
            return `https://ph2.api.riotgames.com/lol/summoner/v4/summoners/by-name/${nameToSearch}?api_key=${apiKey}`;
        }

        /** 
         * Author: Samson, Mik Iris
         * Uses ssbnRequestBuilder method to return the searched profile's summoner id
         * Gets the summoner's encrypted ID by name
         * returns encryptedSummonerID
        **/ 
        async function getSummonerIDByName(summonerName){
            // Search for the summoner by name
            const request = ssbnRequestBuilder(summonerName);
            const responseID = await fetch(request);
            const dataID =  await responseID.json();
            // Extract only the puuid of the summoner
            const encryptedID = dataID.id; 
            console.log(encryptedID);
            return encryptedID;
        }

        /** 
         * Author: Samson, Mik Iris
         * Request for getting the summoner's rank tier via their summoner ID
        **/ 
        async function tierRequestBuilder(region, summonerID){
            return "https://" + region + ".api.riotgames.com/lol/league/v4/entries/by-summoner/" + summonerID + "?api_key=" + apiKey;
        }

        /** 
         * Author: Samson, Mik Iris
         * Returns players' summoner rank tier using the tierRequestBuilder
        **/ 
        async function getSummonerTier(region, summonerID){
            const request = await tierRequestBuilder(region, summonerID);
            const response = await fetch (request);
            const data = await response.json();
            if (data[0] == null && data[1] == null) {
                console.log("unranked");
                return "unranked";
            }
            else if (data[0] == null) {
                console.log(data[1].tier);
                return data[1].tier;
            }
            else {
                console.log(data[0].tier);
                return data[0].tier;
            }
        }

        /** 
         * Author: Samson, Mik Iris
         * Returns a string of the associated puuid of the summoner or player through their username
         * Will be used in a search bar as a query for a method and will be responsive in html
         * Used in searching for a particular player and displaying their stats
        **/ 
        async function getSummonerPUUIDByName(summonerName){
            // Search for the summoner by name
            const request = ssbnRequestBuilder(summonerName);
            const responsePUUID = await fetch(request);
            const dataS =  await responsePUUID.json();
            // Extract only the puuid of the summoner
            const puuid = dataS.puuid; 
            //console.log(puuid);
            return puuid;
        }

        /** 
         * Author: Samson, Mik Iris
         * gets account details via summoner id and given a region
        **/ 
        async function challDetailsBySummIDReq(region, summonerID){
            return "https://" + region + ".api.riotgames.com/lol/summoner/v4/summoners/" + summonerID + "?api_key=" + apiKey;
        }

        /** 
         * Author: Samson, Mik Iris
         * gets the puuid via summonerID instead of by account name
         * This prevents a bug where if a certain player changes their name,
         * their newly changed name wouldn't be recognized by the returned data in the json file
         * summoner ID is absolute compared to the player's username
        **/ 
        async function getPUUIDBySummID(region, summonerID){
            const request = await challDetailsBySummIDReq(region, summonerID);
            const response = await fetch(request);
            const data = await response.json();
            const puuid = data.puuid;
            return puuid;
        }

        /** 
         * Author: Samson, Mik Iris
         * Returns the username or summoner name of the account accurately.
         * This addresses a bug where getting the account by its account name will not be found due to name change.
        **/ 
        async function getNameBySummID(region, summonerID){
            const request = await challDetailsBySummIDReq(region, summonerID);
            const response = await fetch(request);
            const data = await response.json();
            const summName = data.name;
            return summName;
        }

        /**
         * Author: Samson, Mik Iris
         * Request for getting summoner icon via PUUID
         * Used in displaying the top players' icon 
        **/ 
        async function iconRequestBuilder(region, puuid) {
            return "https://" + region + ".api.riotgames.com/lol/summoner/v4/summoners/by-puuid/" + puuid + "?api_key=" + apiKey;
        }

        /**
         * Author: Samson, Mik Iris
         * Gets icon through a request from iconRequestBuilder(region, puuid);
         * icon id is extracted through account puuid
         * icon id is used to get path of the icon using data dragon cdn
        **/ 
        async function getIconByPUUID(region, puuid){
            const request = await iconRequestBuilder(region, puuid);
            const responseIcon = await fetch(request);
            const dataIcon = await responseIcon.json();
            // Extract the id of the summoner's icon
            const summonerIconID = dataIcon.profileIconId;
            const iconPath = await getSummonerIcon(summonerIconID);
            console.log(iconPath);
            return iconPath;
        }

        /**
         * Author: Samson, Mik Iris
         * Gets summoner Icon from Riot CDN to display player icon
         * Uses an icon id to fetch the path of the player's icon to the Riot CDN
        **/
        async function getSummonerIcon(iconID) {
            const iconPath = await `http://ddragon.leagueoflegends.com/cdn/13.20.1/img/profileicon/${iconID}.png`;
            //console.log(iconPath);
            return iconPath;
        }


        /**
         * Author: Samson, Mik Iris
         * Puts the summoner icon of the player alongside their profile
         * In the game, aside from a username, an icon is displayed by the user
         * to identify an account
         * This method is used in displaying the profile of a searched player
         */
        async function putSummonerIcon(summName){
            const puuid = await getSummonerPUUIDByName(summName);
            const iconID = await getIconByPUUID("ph2", puuid);
            const toAddImage = document.getElementById("averageStats");
            const summonerIcon = document.createElement("img");
            summonerIcon.src = iconID;
            summonerIcon.width = 150;
            summonerIcon.height = 150;
            toAddImage.appendChild(summonerIcon);
        }

        /**
         * Author: Samson, Mik Iris
         * This method sorts the list of top League of Legends players by their LP.
         * The given list of top players is randomized and thus it is not sorted by LP.
         * Utilized under getChallengers() to sort the extracted top League of Legends Players in descending order.
        **/
        function compareByLP(player1, player2){

            if(player1.leaguePoints < player2.leaguePoints){
                return 1;
            } 
            if (player1.leaguePoints > player2.leaguePoints){
                return -1;
            }
            return 0;
        }

        /**
         * Author: Samson, Mik Iris
         * Modified: Asuncion, Jonathan
         * Gets the rank emblem images based on the player's rank tier
         * Method identifies the player's rank and displays the respective rank emblem beside their name.
         * Rank determines how good a player is in that certain game mode (flex or solo duo)
         */
        function getRankImagePath(tier) {
            let imagePath;
            switch (tier){
                case "IRON":
                    imagePath = "image/LegRanks/emblem-iron.png";
                    break;
                case "BRONZE":
                    imagePath = "image/LegRanks/emblem-bronze.png";
                    break;
                case "SILVER":
                    imagePath = "image/LegRanks/emblem-silver.png";
                    break;
                case "GOLD":
                    imagePath = "image/LegRanks/emblem-gold.png";
                    break; 
                case "PLATINUM":
                    imagePath = "image/LegRanks/emblem-platinum.png";
                    break;
                case "DIAMOND":
                    imagePath = "image/LegRanks/emblem-diamond.png";
                    break;
                case "MASTER":
                    imagePath = "image/LegRanks/emblem-master.png";
                    break;
                case "GRANDMASTER":
                    imagePath = "image/LegRanks/emblem-grandmaster.png";
                    break;     
                case "CHALLENGER":
                    imagePath = "image/LegRanks/emblem-challenger.png";
                    break;
                case "unranked":
                    imagePath = "unranked"; //? logo for unranked
                    break;                       
                default:
                    break;
            }
            console.log(imagePath);
            return imagePath;
        }

        /**
         * Author: Samson, Mik Iris
         * Modified: Asuncion, Jonathan
         * Uses a specific summoner's PUUID to get match data for past X games
         * Number of games taken into account depends on matchRequestBuilder()
         * Match data will be used to calculate averages for their:
         * assists, kills, deaths, gold earned, minions killed, and kill death assist ratio.
         * Also displays the player's icon and rank
        **/ 
        async function getSummonerMatchDetails(summName, key, numberOfGames, region){
            console.log(region);
            try {
                var playerWins = 0;
                const TOTAL_GAMES = numberOfGames;
                const assistsLog = [];
                const killsLog = [];
                const deathsLog = [];
                const goldLog = [];
                const csLog = [];
                const kdaLog = [];

                const puuid = await getSummonerPUUIDByName(summName);
                const summonerID = await getSummonerIDByName(summName);
                
                const requestBuilder = await matchRequestBuilder(puuid, key, numberOfGames);
                const responseMatches = await fetch(requestBuilder);
                const dataMatches = await responseMatches.json();
                const iconID = await getIconByPUUID(region, puuid);
                const tier = await getSummonerTier(region, summonerID);
                const path = getRankImagePath(tier);

                const playerIcon = document.createElement("img");
                playerIcon.src = iconID;
                playerIcon.width = 100;
                playerIcon.height = 100;
                playerIcon.setAttribute("id",`playerIcon`);
                const toAddStats = document.getElementById("averageStats");
                
                //console.log(dataMatches);

                for (const matchID of dataMatches){

                    const currentMatch = await getMatchData(matchID, key);
                    const playerIndex = currentMatch.metadata.participants.indexOf(puuid);

                    if (playerWon(currentMatch, playerIndex)) {
                        playerWins++;
                    }

                    const assists = currentMatch.info.participants[playerIndex].assists;
                    assistsLog.push(assists);

                    const kills = currentMatch.info.participants[playerIndex].kills;
                    killsLog.push(kills);

                    const deaths = currentMatch.info.participants[playerIndex].deaths;
                    deathsLog.push(deaths);

                    const earnings = currentMatch.info.participants[playerIndex].goldEarned;
                    goldLog.push(earnings);

                    const cs = currentMatch.info.participants[playerIndex].totalMinionsKilled;
                    const ncs = currentMatch.info.participants[playerIndex].neutralMinionsKilled;
                    csLog.push(cs + ncs);

                    var kda; 

                    if (deaths === 0) {
                        kda = kills + assists;
                    }
                    else {
                        kda = (kills + assists) / deaths;
                    }

                    if (isNaN(kda)) {
                        kda = 0;
                    }

                    kdaLog.push(kda);
                }
                console.log(kdaLog);

                if (path != "unranked") {
                    var rankIcon = document.createElement("img");
                    rankIcon.setAttribute("id",`summRank`);
                    rankIcon.src = path;
                    rankIcon.width = 100;
                    rankIcon.height = 100;
                } 

                const wr = calculateWinRate(playerWins, TOTAL_GAMES);
                const avgAssists = average(assistsLog);
                const avgKills = average(killsLog);
                const avgDeaths = average(deathsLog);
                const avgEarnings = average(goldLog);
                const avgCS = average(csLog);
                const avgKDA = average(kdaLog);

                
                console.log("Winrate: " + wr + "%");
                console.log("Average Assists: " + avgAssists);
                console.log("Average Kills: " + avgKills);
                console.log("Average Deaths: " + avgDeaths);
                console.log("Average Gold Earned: " + avgEarnings);
                console.log("Average CS: " + avgCS);
                console.log("Average KDA: " + avgKDA);

                const tableBody = document.getElementById("playerTable");
                while (tableBody.firstChild) {
                    tableBody.removeChild(tableBody.firstChild);
                }

                var header = document.createElement("tr");
                sumH = document.createElement("h1");
                sumH.innerHTML = "Summoner";

                statH = document.createElement("h1");
                statH.innerHTML = "Statistics";

                var tdH = document.createElement("td");
                var tdHh = document.createElement("td");
                tdH.setAttribute("id", "summ");
                tdHh.setAttribute("id","stat");
                tdH.appendChild(sumH);
                tdHh.appendChild(statH);

                header.appendChild(tdH);
                header.appendChild(tdHh);
                tableBody.appendChild(header);

                
                var td1 = document.createElement("td");
                var td2 = document.createElement("td");

                var h1SummonerName = document.createElement("h1");
                h1SummonerName.textContent = summName;

                td1.appendChild(playerIcon);
                if(path != "unranked") {td1.appendChild(rankIcon);}
                td1.appendChild(h1SummonerName);
            

                var h1WinRate = document.createElement("h1");
                h1WinRate.textContent ="Winrate: "+ wr + "%";
                var AvgKills = document.createElement("h3");
                AvgKills.textContent ="Average Kills: " +  avgKills.toFixed(2);
                var AvgDeaths = document.createElement("h3");
                AvgDeaths.textContent ="Average Deaths: " + avgDeaths.toFixed(2);
                var AvgAssists = document.createElement("h3");
                AvgAssists.textContent = "Average Assists: " + avgAssists.toFixed(2);
                var AvgEarnings = document.createElement("h3");
                AvgEarnings.textContent ="Average Gold Earned: " + avgEarnings.toFixed(2);
                var AvgCS = document.createElement("h3");
                AvgCS.textContent ="Average CS: " + avgCS.toFixed(2);
                var AvgKDA = document.createElement("h3");
                AvgKDA.textContent ="Average KDA: " + avgKDA.toFixed(2);

                td2.appendChild(h1WinRate);
                td2.appendChild(AvgKills);
                td2.appendChild(AvgDeaths);
                td2.appendChild(AvgAssists);
                td2.appendChild(AvgEarnings);
                td2.appendChild(AvgCS);
                td2.appendChild(AvgKDA);

                var row = document.createElement("tr");
                row.appendChild(td1);
                row.appendChild(td2);

                
                console.log("second child occurence");
                tableBody.appendChild(row);
            }

            catch(error){
                console.error("Request limit is being reached by RIOT API; please wait for a moment.", error);
            }

        } // end of getSummonerMatchDetails(); 

        /**
         * Author: Samson, Mik Iris
         * Request for getting last 20 matches of a summoner through PUUID
         * Utilized in  getSummonerMatchDetails(summName, key, numberOfGames, region)
        **/ 
        async function matchRequestBuilder(puuid, key, numberOfGames) {
            return "https://sea.api.riotgames.com/lol/match/v5/matches/by-puuid/" + puuid + "/ids?start=0&count=" + numberOfGames + "&api_key=" + key;
        }
        
        /**
         * Author: Samson, Mik Iris
         * Gets the data and metrics of a single non custom League of Legends match/game given a match ID
        **/ 
        async function getMatchData(matchID, key){
            const request = "https://sea.api.riotgames.com/lol/match/v5/matches/" + matchID + "?api_key=" + key;
            const responseMatch = await fetch(request);
            const dataMatch = await responseMatch.json();
            return dataMatch;
        }

        /**
         * Author: Samson, Mik Iris
         * Gets the data and metrics of a single non custom League of Legends match/game given a match ID
         * Checks if a player won in a certain match
        **/ 
        function playerWon(thisMatch, playerIndex){
            if (thisMatch.info.participants[playerIndex].win == true) {
                return true;
            }
        }

        /**
         * Author: Samson, Mik Iris
         * Calculates an average value of a given array
         * Used in displaying the average stats of a user in the getSummonerMatchDetails method
        **/ 
        function average(array){
            if (array.length === 0) {
                return 0;
            }
            const sum = array.reduce((accumulator, currentValue) => accumulator + currentValue, 0);
            const mean = sum / array.length;
            return mean;
        }

        /**
         * Author: Samson, Mik Iris
         * Calculates a player's Win Rate over a total number of games (defaulted by 20)
        **/ 
        function calculateWinRate(wins, totalGames) {
            if (totalGames === 0) {
                return 0;
            }

            const winRate = (wins / totalGames) * 100;
            return winRate.toFixed(2); // 2 decimal places
        }
