// SEARCH BAR FUNCTION -------------------------------------
const search = () =>{
  const searchbox = document.getElementById("search-event").value.toUpperCase();
  const school_events = document.getElementById("events");
  const events = document.querySelectorAll(".grid-item");
  const ename = school_events.getElementsByTagName("h1");

  for (var i = 0; i < ename.length; i++){
    let match = events[i].getElementsByTagName("h1")[0]

    if (match){
      let textvalue = match.textContent || match.innerHTML;

      if(textvalue.toUpperCase().indexOf(searchbox) > -1){
        events[i].style.display = "";
      } else {
        events[i].style.display = "none";
      }
    }
  }
}


const openModalButtons = document.querySelectorAll('[data-modal-target]')
const closeModalButtons = document.querySelectorAll('[data-close-button]')
const overlay = document.getElementById('overlay')

openModalButtons.forEach(button => {
  button.addEventListener('click', () => {

    const modal = document.querySelector(button.dataset.modalTarget)
    openModal(modal)
  })
})

overlay.addEventListener('click', () => {
  const modals = document.querySelectorAll('.modal.active')
  modals.forEach(modal => {
    closeModal(modal)
  })
})

closeModalButtons.forEach(button => {
  button.addEventListener('click', () => {
    const modal = button.closest('.modal')
    closeModal(modal)
  })
})



function openModal(modal) {
  if (modal == null) return

  modal.classList.add('active')
  overlay.classList.add('active')

} 

function closeModal(modal) {
  if (modal == null) return
  modal.classList.remove('active')
  overlay.classList.remove('active')
}

function get_id(clicked_id){
  document.getElementById("id").innerText = clicked_id;
}


function showPopup() {
  document.getElementById('imagePopup').style.display = 'block';
}

function hidePopup() {
  document.getElementById('imagePopup').style.display = 'none';
}