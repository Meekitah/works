<header>
    <div class="row">
        <div id="columnLogo">
            <img id="sluLogo" src="images/SLU Logo.png" alt="SLU logo">
        </div>
        <div id="columnText">
            <h1>SAINT LOUIS UNIVERSITY EVENT PORTAL</h1>
            <h3>Baguio City, Philippines</h3>
        </div>
        </row>
    </div>
    <div class="blueBlock">
        <ul>
        <li><a href="index.php">Home</a></li>
        <li><a href="about_us.php">About us</a></li>
        <?php
        if(!isset($_SESSION["user_id"])){ ?>
        <a class="form-btn" href="login.php" class="button" style="float:right">Login</a>
        <a class="form-btn" href="signup.php" class="button" style="float:right">Signup</a>

        <?php } else { ?>
            <li><a href="my_events.php">My Events</a></li>
            <li><a href="update.php">Update Details</a></li>
            <li id="openForm" style="float:right"> Welcome <?php echo $_SESSION["user_firstname"];?> </li>
            <li>
            <form action="includes/logout.inc.php" method="post">
                <button class = "form-button" id = "logout">Logout</button>
            </form>
            </li>
        <?php }?>
        </ul>
    </div>    

</header>
