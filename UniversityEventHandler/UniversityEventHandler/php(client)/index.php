<?php
require_once 'includes/config.php';
require_once "includes/events/events_view.inc.php";
?>


<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>SLU EVENT PORTAL</title>
  <link href="css/event_style.css" rel="stylesheet">
  <script defer src="js/script.js"></script>

</head>
<body>

    <?php 
    include('templates/header.php'); 
    include('includes/events/public_homepage.inc.php');
    include('templates/footer.php'); 
    ?>

</body>
</html>