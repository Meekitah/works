<?php

declare(strict_types=1);


class my_events{
    public $event_name;
    public $event_date;
    public $status;
    public $qr;
}

class my_schedules{
    public $event_name;
    public $scheduleID;
    public $description;
    public $venue;
    public $date;
    public $status;
    public $QR;
}

function get_my_events(object $pdo, int $userID){
    $query = "SELECT event.eventid ,event_name, date, status, QR
                FROM user
                INNER JOIN event_registry
                ON user.userID = event_registry.userID
                INNER JOIN event
                ON event_registry.eventID = event.eventid
                WHERE user.userID = :userID;";

    $stmt = $pdo->prepare($query);
    $stmt->bindParam(":userID", $userID);
    $stmt->execute();

    $result = $stmt->fetchAll(\PDO::FETCH_CLASS, "my_events");
    return $result;
}

function get_my_schedules(object $pdo, int $userID){
    $query = "SELECT event.event_name, schedule.scheduleID,  schedule.description , schedule.venue, 
                schedule.date, schedule.status, QR
                FROM user
                INNER JOIN event_registry
                ON user.userID = event_registry.userID
                INNER JOIN event 
                ON event_registry.eventID = event.eventid
                INNER JOIN schedule
                ON event_registry.schedID = schedule.scheduleID
                WHERE user.userID = :userID;";

    $stmt = $pdo->prepare($query);
    $stmt->bindParam(":userID", $userID);
    $stmt->execute();

    $result = $stmt->fetchAll(\PDO::FETCH_CLASS, "my_schedules");
    return $result;
}

function delete_my_event(object $pdo, int $userID, int $schedID){
    $query = "DELETE FROM event_registry WHERE userID = :userID AND schedID = :schedID;";

    $stmt = $pdo->prepare($query);
    $stmt->bindParam(":userID", $userID);
    $stmt->bindParam(":schedID", $schedID);
    $stmt->execute();

    $result = $stmt->fetch(\PDO::FETCH_CLASS);
    return $result;
}
