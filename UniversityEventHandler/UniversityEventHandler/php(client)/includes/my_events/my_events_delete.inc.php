<?php

declare(strict_types=1);


if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $userID = $_POST["userID"];
    $schedID = $_POST["schedID"];
    $text = $userID . $schedID;


    try{
        require_once '../dbh.inc.php';
        require_once 'my_events_model.inc.php';

    
        delete_my_event($pdo, (int)$userID, (int)$schedID,$QR);

        header("Location: ../../my_events.php?delete=success");

        $pdo = null;
        $stmt = null;   

        die();
    } catch (PDOException $e){
        die("Query failed: " . $e->getMessage());
    }

} else{
    header("Location: ../../my_events.php");
    die();
}