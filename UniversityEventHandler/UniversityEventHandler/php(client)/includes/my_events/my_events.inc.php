<?php

declare(strict_types=1);

try{

require_once "includes/dbh.inc.php";
require_once "includes/my_events/my_events_model.inc.php";
require_once "includes/events/events_model.inc.php";


$userID = (int)$_SESSION["user_id"];
$events = get_my_schedules($pdo, $userID);


} catch (PDOException $e){
  die("Query failed: " . $e->getMessage());
}
?>

<div id="table-div">
                <table class="eventsTable">
                    <thead>
                        <tr class="table-heads">
                            <th>Event Name</th>
                            <th>Sched ID</th>
                            <th>Sched Description</th>
                            <th>Venue</th>
                            <th>Date</th>
                            <th>Status</th>
                            <th>QR</th>
                        </tr>
                    </thead>
                    <tbody>
    <?php  foreach ($events as $event ) { 
        ?>

            <tr>
                <td class="event-title" style="font-weight: bold;"><?php echo $event->event_name ?></td>
                <td class="event-title" style="font-weight: bold;"><?php echo $event->scheduleID ?></td>
                <td class="event-number"><?php echo $event->description ?></td>
                <td class="event-title" style="font-weight: bold;"><?php echo $event->venue ?></td>
                <td class="event-date"><?php echo $event->date ?></td>
                <td class="status"><button><?php echo $event->status ?></button></td>
                <td class="qr">
                    <form method="post" action="includes/my_events/show_qr.php">
                        <input type="hidden" name="text" value = "<?php echo $event->QR ?>">
                    <button>Show QR</button>
                    </form>
                </td>
            </tr>

            
    <?php } ?> 
</div>

<h3>If you want to delete a registered event, write down the number and delete</h3>
<form method="post" action="includes/my_events/my_events_delete.inc.php">
    <input hidden  type="text" name="userID"  value = "<?php echo $_SESSION["user_id"] ?>" >
    <input type="text" name="schedID" >
    <button>Delete</button>
</form>



    