<?php

declare(strict_types=1);

function is_input_empty(string $firstname,string $lastname,string $pwd,string $email,string $org,$type) {
    if (empty($firstname) || empty($lastname) || 
            empty($pwd) || empty($email) ||
            empty($org) || empty($type) ){
                return true;
    } else {
        return false;
    }
}

function is_input_empty_update(string $firstname,string $lastname,string $pwd,string $email,string $org) {
    if (empty($firstname) || empty($lastname) || 
            empty($pwd) || empty($email) ||
            empty($org)){
                return true;
    } else {
        return false;
    }
}

function is_email_invalid(string $email) {
    if (!(filter_var($email, FILTER_VALIDATE_EMAIL))){
        return true;
    } else {
        return false;
    }
}

function is_username_taken(object $pdo,string $firstname,string $lastname) {
    if (get_name($pdo, $firstname, $lastname)){
        return true;
    } else {
        return false;
    }
}

function is_email_registered(object $pdo, string $email) {
    if (get_email($pdo, $email)){
        return true;
    } else {
        return false;
    }
}

function create_user(object $pdo, string $firstname,string $lastname,string $pwd,string $email,string $org,$type){
    set_user($pdo,$firstname,$lastname,$pwd,$email,$org,$type);
}