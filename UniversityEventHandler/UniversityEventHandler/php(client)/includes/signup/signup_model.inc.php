<?php

declare(strict_types=1);

function get_name(object $pdo, string $firstname, string $lastname){
    $query = "SELECT CONCAT(firstname, ' ' , lastname) FROM user WHERE CONCAT(firstname, ' ' , lastname) = :completename;";
    $stmt = $pdo->prepare($query);
    $completename = $firstname . " " . $lastname;
    $stmt->bindParam(":completename", $completename);
    $stmt->execute();

    $result = $stmt->fetch(PDO::FETCH_ASSOC);
    return $result;
}

function get_email(object $pdo, string $email){
    $query = "SELECT email FROM user WHERE email = :email;";
    $stmt = $pdo->prepare($query);
    $stmt->bindParam(":email", $email);
    $stmt->execute();

    $result = $stmt->fetch(PDO::FETCH_ASSOC);
    return $result;
}

function set_user(object $pdo, string $firstname,string $lastname,string $pwd,string $email,string $org,$type){

    $query = "INSERT INTO user ( email, password, firstname, lastname,
    organizations, type) VALUES ( :email, :password, :firstname, 
    :lastname, :organizations, :type);";
    $stmt = $pdo->prepare($query);

    $stmt->bindParam(":email", $email);
    $stmt->bindParam(":password", $pwd);
    $stmt->bindParam(":firstname", $firstname);
    $stmt->bindParam(":lastname", $lastname);
    $stmt->bindParam(":organizations", $org);
    $stmt->bindParam(":type", $type);

    $stmt->execute();
}

function update_user(object $pdo, string $firstname,string $lastname,string $pwd,string $email,string $org){

    $query = "UPDATE user SET email = :email
            , password = :password, firstname = :firstname
            , lastname = :lastname, organizations = :organizations
            WHERE userID = :userID" ;

        $stmt = $pdo->prepare($query);

        $stmt->bindParam(":email", $email);
        $stmt->bindParam(":password", $pwd);
        $stmt->bindParam(":firstname", $firstname);
        $stmt->bindParam(":lastname", $lastname);
        $stmt->bindParam(":organizations", $org);
        $stmt->bindParam(":userID", $_SESSION["user_id"]);

        $stmt->execute();
}