<?php



if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $firstname = $_POST["firstname"];
    $lastname = $_POST["lastname"];
    $pwd = $_POST["pwd"];
    $email = $_POST["email"];
    $org = $_POST["org"];
    $type = $_POST["type"];


    try{
        require_once "../dbh.inc.php";
        require_once "signup_model.inc.php";
        require_once "signup_contr.inc.php";

        $errors = [];

        // ERROR HANDLERS
        if (is_input_empty($firstname, $lastname,$pwd,$email,$org,$type)){
            $errors ["empty_input"] = "Fill in all fields!";
        }
        if (is_email_invalid($email)){
            $errors ["invalid_email"] = "Invalid email used!";
        }
        if (is_username_taken($pdo,$firstname,$lastname)){
            $errors ["name_taken"] = "Name already taken!";
        }
        if (is_email_registered($pdo, $email)){
            $errors ["email_used"] = "Email already registered!";
        }

        require_once "../config.php";

        if ($errors){
            $_SESSION["error_signup"] = $errors;

            $signupData = [
                "completename" => $firstname . ' ' . $lastname,
                "email" => $email
            ];

            $_SESSION["signup_data"] = $signupData;
            
            header("Location: ../../signup.php");
            die();
        }

        create_user($pdo,$firstname,$lastname,$pwd,$email,$org,$type);
        
        header("Location: ../../signup.php?signup=success");

        $pdo = null;
        $stmt = null;

        die();


    } catch (PDOException $e){
        die("Query failed: " . $e->getMessage());
    }

    header("Location: ../index.php?signup=success");
}else{
    header("Location: ../../signup.php");
}