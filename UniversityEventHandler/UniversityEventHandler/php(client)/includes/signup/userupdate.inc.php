<?php



if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $firstname = $_POST["firstname"];
    $lastname = $_POST["lastname"];
    $pwd = $_POST["pwd"];
    $email = $_POST["email"];
    $org = $_POST["org"];


    try{
        require_once "../dbh.inc.php";
        require_once "signup_model.inc.php";
        require_once "signup_contr.inc.php";

        $errors = [];

        // ERROR HANDLERS
        if (is_input_empty_update($firstname, $lastname,$pwd,$email,$org)){
            $errors ["empty_input"] = "Fill in all fields!";
        }
        if (is_email_invalid($email)){
            $errors ["invalid_email"] = "Invalid email used!";
        }
        if (is_username_taken($pdo,$firstname,$lastname)){
            $errors ["name_taken"] = "Name already taken!";
        }
        if (is_email_registered($pdo, $email)){
            $errors ["email_used"] = "Email already registered!";
        }

        require_once "../config.php";

        if ($errors){
            $_SESSION["error_signup"] = $errors;

            $updateData = [
                "completename" => $firstname . ' ' . $lastname,
                "email" => $email
            ];

            $_SESSION["update_data"] = $updateData;
            
            header("Location: ../../update.php");
            die();
        }


        update_user($pdo,$firstname,$lastname,$pwd,$email,$org);

        $_SESSION["user_firstname"] = htmlspecialchars($firstname);
        $_SESSION["user_email"] = htmlspecialchars($email);

        header("Location: ../../update.php?update=success");

        $pdo = null;
        $stmt = null;
        
        

        die();


    } catch (PDOException $e){
        die("Query failed: " . $e->getMessage());
    }

    header("Location: ../../update.php");
}
else{
    header("Location: ../../update.php");
}