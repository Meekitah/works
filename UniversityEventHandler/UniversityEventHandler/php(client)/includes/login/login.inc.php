<?php

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $email = $_POST["email"];
    $pwd = $_POST["pwd"];

    try{
        require_once '../dbh.inc.php';
        require_once 'login_model.inc.php';
        require_once 'login_contr.inc.php';

        $errors = [];
        // ERROR HANDLERS
        if (is_input_empty($email,$pwd)){
            $errors ["empty_input"] = "Fill in all fields!";
        }
        
        $result =get_email($pdo, $email);

        if (is_email_wrong($result)){
            $errors ["login_incorrect"] = "Incorrect login info!";
        }
        if (!is_email_wrong($result) && is_password_wrong($pwd, $result["password"])){
            $errors ["login_incorrect"] = "Incorrect login info!";
        }


        require_once "../config.php";

        if ($errors){
            $_SESSION["errors_login"] = $errors;

            header("Location: ../../login.php");
            die();
        }

        $newSessionId = session_create_id();
        $sessionId = $newSessionId . "_" . $result["usersID"];
        session_id($sessionId);
        
        $_SESSION["user_id"] = htmlspecialchars($result["userID"]) ;
        $_SESSION["user_firstname"] = htmlspecialchars($result["firstname"]);
        $_SESSION["user_lastname"] = htmlspecialchars($result["lastname"]);
        $_SESSION["user_password"] = htmlspecialchars($result["password"]);
        $_SESSION["user_email"] = htmlspecialchars($result["email"]);
        $_SESSION["user_org"] = htmlspecialchars($result["organizations"]);

        $_SESSION['last_regeneration'] = time();

        header("Location: ../../index.php?login=success");

        $pdo = null;
        $stmt = null;   

        die();
    } catch (PDOException $e){
        die("Query failed: " . $e->getMessage());
    }

} else{
    header("Location: ../../index.php");
    die();
}