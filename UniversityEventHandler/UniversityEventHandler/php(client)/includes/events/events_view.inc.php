<?php

declare(strict_types=1);


function check_register_errors(){
    if(isset($_SESSION["error_register"])){
        $errors = $_SESSION["error_register"];

        foreach ($errors as $error) {
            echo '<p class = "labels" id ="red">' . $error . '</p>';
        }

        unset($_SESSION['error_register']);
    } else if(isset($_SESSION["success_register"])) {
        echo '<p class = "labels">' . $_SESSION['success_register'] . '</p>';
        unset($_SESSION["success_register"]);
    }

    unset($_SESSION['error_register']);
    unset($_SESSION["success_register"]);
}