<?php

declare(strict_types=1);

require "vendor/autoload.php";
require_once "../dbh.inc.php";
require_once 'events_model.inc.php';
require_once 'events_contr.inc.php';

use Endroid\QrCode\QrCode;
use Endroid\QrCode\Writer\PngWriter;
use Endroid\QrCode\Label\Label;




if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $userID = $_POST["userID"];
    $eventID = $_POST["eventID"];
    $schedID = $_POST["schedID"];
    $text = $userID . $eventID. $schedID;

    $errors = [];

        if (is_registered($pdo, (int)$userID, (int)$eventID, (int)$schedID)){
            $errors ["register_error"] = "You have already registered in this schedule";
        }

        require_once "../config.php";

        if ($errors){
            $_SESSION["error_register"] = $errors;

            header("Location: ../../index.php");
            die();
        } else {
            $_SESSION["success_register"] = "You have successfully registered in this schedule";
        }

    try{
        
        $qr_code = QrCode::create($text);

        $label = Label::create($userID."-".$eventID."-".$schedID);

        $writer = new PngWriter;

        $result = $writer->write($qr_code, null, $label);

        $QR = $result->getString();


        register_event($pdo, (int)$userID, (int)$eventID,(int)$schedID,base64_encode($QR));

        header("Location: ../../index.php?register=success");

        $pdo = null;
        $stmt = null;   

        die();
    } catch (PDOException $e){
        die("Query failed: " . $e->getMessage());
    }

} else{
    header("Location: ../../index.php");
    die();
}