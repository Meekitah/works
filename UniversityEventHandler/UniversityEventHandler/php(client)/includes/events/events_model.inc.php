<?php

declare(strict_types=1);


class events{
    public $eventid;
    public $event_name;
    public $description;
    public $fee;
    public $organization;
    public $date;
    public $date_created;
    public $status;
    public $poster;
}

class schedule{
    public $scheduleID;
    public $eventID;
    public $desc;
    public $venue;
    public $date;
    public $status;
}

function search_schedule(object $pdo, string $eventID){
    $query = "SELECT * FROM schedule WHERE eventID = :eventID" ;

    $stmt = $pdo->prepare($query);
    $stmt->bindParam(":eventID", $eventID);
    $stmt->execute();

    $result = $stmt->fetchAll(\PDO::FETCH_CLASS, "schedule");
    return $result;
}


function get_events(object $pdo){
    $query = "SELECT * FROM event";
    $stmt = $pdo->prepare($query); 
    $stmt->execute();

    $result = $stmt->fetchAll(\PDO::FETCH_CLASS, "events");
    return $result;
}

function get_registered_events(object $pdo, int $userID){
    $query = "SELECT * FROM event_registry WHERE userID = :userID;";
    $stmt = $pdo->prepare($query);
    $stmt->bindParam(":userID", $userID);
    $stmt->execute();

    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    return $result;
}

function get_registered_event(object $pdo, int $userID, int $eventID, int $schedID){
    $query = "SELECT eventID FROM event_registry WHERE userID = :userID AND eventID = :eventID AND schedID = :schedID;";
    $stmt = $pdo->prepare($query);
    $stmt->bindParam(":userID", $userID);
    $stmt->bindParam(":eventID", $eventID);
    $stmt->bindParam(":schedID", $schedID);
    $stmt->execute();

    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    return $result;
}

function get_event(object $pdo, int $eventid){
    $query = "SELECT * FROM event WHERE eventid = :eventid;";
    $stmt = $pdo->prepare($query);
    $stmt->bindParam(":eventid", $eventid);
    $stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_CLASS,"events");
    return $result;
}

function register_event(object $pdo, int $userID, int $eventID, int $schedID, string $QR){
    $query = "INSERT INTO event_registry ( userID, eventID, schedID, event_registered, QR) 
                VALUES ( :userID, :eventID, :schedID,:event_registered, :QR);";
    $stmt = $pdo->prepare($query);
    $t=time();

    $stmt->bindParam(":userID", $userID);
    $stmt->bindParam(":eventID", $eventID);
    $stmt->bindParam(":schedID", $schedID);
    $stmt->bindParam(":event_registered",date("Y-m-d",$t) );
    $stmt->bindParam(":QR", $QR);
    
    $stmt->execute();
}

function delete_images(){
    $folderPath = "images/generated";
    
    $folder = opendir($folderPath);
    
    while (($file = readdir($folder)) !== false) {
        $filePath = $folderPath . "/" . $file;

        if (is_file($filePath)) {
            unlink($filePath);
        }
    }

    closedir($folder);
}