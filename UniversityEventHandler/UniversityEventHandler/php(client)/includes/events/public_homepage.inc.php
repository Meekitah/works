<?php

declare(strict_types=1);

try{
require_once "includes/dbh.inc.php";
require_once "includes/events/events_model.inc.php";
require_once "includes/events/events_view.inc.php";

$events = get_events($pdo);
delete_images();

} catch (PDOException $e){
  die("Query failed: " . $e->getMessage());
}
?>


  <div id="blur-div">
  
  </div>
  
  <?php 
    check_register_errors();
  ?>

  <h3 class="labels" >Search for event title</h3>
  <form>
    <input type="text" name="" id="search-event" placeholder="Search events"
    onkeyup="search()" >
  </form>

 
  <h3 class="labels">Click on the events to look at the details</h3>
  <div class="events" id="events">
    <div class="grid">
    <?php  foreach ($events as $event ) { ?>
        
        <div id = <?php echo $event->eventid ?> class="grid-item limit" data-modal-target="#modal" 
                  data-name = "<?php echo $event->event_name ?>"
                  data-desc = "<?php echo $event->description ?>"
                  data-fee = "<?php echo $event->fee ?>"
                  data-org = "<?php echo $event->organization ?>"
                  data-date = "<?php echo $event->date ?>"
                  data-stat = "<?php echo $event->status ?>"
                  data-post = "<?php echo $event->poster ?>"
                  onclick="change_modal_details(this.id)">


        <?php 
          if(is_string($event->poster)){
          $imageData = base64_decode($event->poster);
          $imagePath = "images/generated/". $event->event_name .".jpg";
          file_put_contents($imagePath, $imageData);
        ?>
          <img src=" <?php echo $imagePath?>" > 
        <?php } else { ?>
          <img src="" >
        <?php } ?>


        <h1 class="event_name">
          <?php echo $event->event_name ?>
        </h1>
        <span class="text">
          <?php echo $event->description ?>
        </span>
        </div>

        <!-- SCHEDULES ----------------------------------- -->
        <?php $sched_identifier = "sched". $event->eventid?>

        <div id= <?php echo $sched_identifier ?> hidden>
          <?php  
            $schedules = search_schedule($pdo, $event->eventid);
            foreach ($schedules as $schedule ) { 
              ?>
              <div id ="sched">
                <h1 id = "venue">
                  <?php echo $schedule->venue ?>
                </h1>
                <h2 id = "date">
                  <?php echo $schedule->date ?>
                </h2>
                <p id="sched_desc">
                  <?php echo $schedule->description ?>
                </p>
                <h3 id = "status">
                  <?php echo $schedule->status ?>
                </h3>
                <?php
                if(isset($_SESSION["user_id"])){ 
                    ?>
                    <form method="post" action="includes/events/events.inc.php">
                      <input hidden type="text" name="userID"  value = "<?php echo $_SESSION["user_id"] ?>" >
                      <input hidden type="text" name="eventID"  id = "eventID" value = <?php echo $event->eventid ?>>
                      <input hidden type="text" name="schedID"  value = "<?php echo $schedule->scheduleID ?>" >
                      <button class = "sched-button" >Register</button>
                    </form>
                <?php }?>
              </div>

              

          <?php } ?>
        </div>
      <?php } ?>

    </div>

  </div>

  <p id="test"></p>

    <div class="modal" id="modal">
      <div class="modal-header" id="head">
        <div>Event Details</div>
        <button data-close-button class="close-button">&times;</button>
      </div>
        <div id="details-body">
          <h1 class="title" id="title"></h1>
          <h4 id="organization"></h4>
          <h2 id="date"></h2>
          <h3 id="fee"></h3>
          <h4 id="status"></h4>
          <div id="date"></div>
          <p class="modal-body" id="description"></p>
        </div>
        <h1 id="sched">Schedule</h1>
        <div id="schedules">
        </div>
    </div>
    <div id="overlay"></div>

<p id="test" ></p>

<script>s

  function change_modal_details(hover_id){
    let sched = "sched" + hover_id;
    
    const event = document.getElementById(hover_id);
    document.getElementById("title").innerText = event.dataset.name;
    document.getElementById("description").innerText = event.dataset.desc;
    document.getElementById("fee").innerText = "fee: " + event.dataset.fee;
    document.getElementById("organization").innerText = event.dataset.org;
    document.getElementById("date").innerText = event.dataset.date;
    document.getElementById("status").innerText = event.dataset.stat;
    document.getElementById("schedules").innerHTML = document.getElementById(sched).innerHTML;
}

</script>