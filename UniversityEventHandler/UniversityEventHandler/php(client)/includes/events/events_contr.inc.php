<?php

declare(strict_types=1);


function is_registered(object $pdo, int $userID, int $eventID, int $schedID) {
    if (get_registered_event($pdo, $userID, $eventID, $schedID)){
        return true;
    } else {
        return false;
    }
}
