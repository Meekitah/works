<?php
require_once 'includes/config.php';
require_once 'includes/signup/signup_view.inc.php';
?>

<!DOCTYPE html>
<html lang="en">
<title>Signup</title>
<body>

    <?php 
    include('templates/head.php');
    include('templates/header.php'); ?>


    <div class="form-template" id="registerForm">
    <div class="form-container" id="registerForm">
        <form class="form-container" action="includes/signup/signup.inc.php" method="post">
              <div class="row">
                <img id="sluLogo" src="images/SLU Logo.png" alt="SLU Logo">
              </div>
              <div class="row">
              <input type="varchar" name="firstname" placeholder = "Firstname">
              </div>
              <div class="row">
              <input type="varchar" name="lastname" placeholder = "Lastname">
              </div>
              <div class="row">
              <input type="varchar" name="pwd" placeholder = "Password">
              </div>
              <div class="row">
              <input type="varchar" name="email" placeholder = "Email">
              </div>
              <div class="row">
              <input type="varchar" name="org" placeholder = "Organization">
              </div>
              <div class="row">
                <select id="type" name = "type">
                <option value="regular">Regular</option>
                <option value="admin">Admin</option>
                </select>   
              </div>
              <div class="button-container">
                <button class="btn" id="form-button">Register</button>
              </div>
        </form>
            <?php 
          check_signup_errors();?>
    </div>
    </div>
    <?php 
          include('templates/footer.php'); 
      ?>
</body>