<?php
require_once 'includes/config.php';
require_once 'includes/signup/signup_view.inc.php';
?>

<!DOCTYPE html>
<html lang="en">
<title>Update</title>
<body>

    <?php 
    include('templates/head.php');
    include('templates/header.php'); ?>;

<div class="form-template" id="registerForm">
    <div class="form-container">
        <form class="form-container" action="includes/signup/userupdate.inc.php" method="post">
              <div class="row">
                <img id="sluLogo" src="images/SLU Logo.png" alt="SLU Logo">
              </div>
              <div class="row">
              <input type="varchar" name="firstname" placeholder = "Firstname" value = <?php echo $_SESSION["user_firstname"] ?>>
              </div>
              <div class="row">
              <input type="varchar" name="lastname" placeholder = "Lastname" value = <?php echo $_SESSION["user_lastname"] ?> >
              </div>
              <div class="row">
              <input type="varchar" name="pwd" placeholder = "Password" value = <?php echo $_SESSION["user_password"] ?> >
              </div>
              <div class="row">
              <input type="varchar" name="email" placeholder = "Email" value = <?php echo $_SESSION["user_email"] ?> >
              </div>
              <div class="row">
              <input type="varchar" name="org" placeholder = "Organization" value = <?php echo $_SESSION["user_org"] ?>>
              </div>
              <div class="button-container">
                <button class="btn" id="form-button">Update</button>
              </div>
        </form>
            <?php 
          check_signup_errors();?>
    </div>
    </div>
    <?php 
          include('templates/footer.php'); 
      ?>
</body>