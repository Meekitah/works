<?php
require_once 'includes/config.php';
?>

<!DOCTYPE html>
<html lang="en">
  <title>About Us</title>

<body>
    <?php 
    include('templates/head.php');
    include('templates/header.php'); ?>

      <h2 style="text-align:center">Our Team</h2>
      <div class="row">
        <div class="column">
          <div class="card">
            <div class="image-container">
              <img src="images\team pictures\Jonathan.png" alt="Jonathan" >
            </div>
            <div class="container">
              <h2>Jonathan Asuncion</h2>
              <p>2220158@slu.edu.ph</p>
            </div>
          </div>
        </div>
      
        <div class="row">
          <div class="column">
            <div class="card">
              <div class="image-container">
              <img src="images\team pictures\Val.jpg" alt="Valjunyor" >
              </div>
              <div class="container">
                <h2>Valjunyor Alfiler</h2>
                <p>2223905@slu.edu.ph</p>
              </div>
            </div>
          </div>
        
          <div class="row">
            <div class="column">
              <div class="card">
                <div class="image-container">
                <img src="images\team pictures\Aivein.jpg" alt="Aivein">
                </div>
                <div class="container">
                  <h2>Aivein Casibang</h2>
                  <p>2220909@slu.edu.ph</p>
                </div>
              </div>
            </div>

            <div class="row">
                <div class="column">
                  <div class="card">
                    <div class="image-container">
                    <img src="images\team pictures\Gerwin.jpg" alt="Gerwin">
                    </div>
                    <div class="container">
                      <h2>Gerwin Dogui-is</h2>
                      <p>2222719@slu.edu.ph</p>
                    </div>
                  </div>
                </div>

                <div class="row">
                    <div class="column">
                      <div class="card">
                        <div class="image-container">
                        <img src="images\team pictures\Destin.jpg" alt="Destin">
                        </div>
                        <div class="container">
                          <h2>Destin Dupiano</h2>
                          <p>2202529@slu.edu.ph</p>
                        </div>
                      </div>
                    </div>


                    <div class="row">
                        <div class="column">
                          <div class="card">
                            <div class="image-container">
                            <img src="images\team pictures\Kim.png" alt="Mik">
                            </div>
                            <div class="container">
                              <h2>Mik Samson</h2>
                              <p>2222721@slu.edu.ph</p>
                            </div>
                          </div>
                        </div>

      </div>
      <?php 
          include('templates/footer.php'); 
      ?>
</body>