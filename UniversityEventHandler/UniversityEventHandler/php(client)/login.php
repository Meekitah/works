<?php
require_once 'includes/config.php';
require_once 'includes/login/login_view.inc.php';
?>

<!DOCTYPE html>
<html lang="en">
<title>Login</title>
<body>

    <?php 
    include('templates/head.php');
    include('templates/header.php'); ?>


  <div class="form-template" id="loginForm">
    <div class="form-container">
        <form class="form-container" action="includes/login/login.inc.php" method="post">
              <div class="row">
                <img id="sluLogo" src="images/SLU Logo.png" alt="SLU Logo">
              </div>
              <div class="row">
                <input type="varchar" placeholder="Enter email" name="email" >
              </div>
              <div class="row">
                <input type="varchar" placeholder="Enter Password" name="pwd" >
              </div>
              <div class="button-container">
                <button class="btn" id="form-button">Login</button>
              </div>
        </form>
            <?php 
          check_login_errors();?>
    </div>
  </div>
  <?php 
          include('templates/footer.php'); 
      ?>
</body>