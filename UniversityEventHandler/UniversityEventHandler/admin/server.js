/**
 * Code author of server.js: Samson, Mik Iris G.
*/

// Import statements
import express from "express";

import { getAllEvents, getEventByID, getEventByName,
     createEvent, deleteEventWithID, deleteEventWithName,
     deleteCompletedEvents, getEventSchedules, getScheduleDetails,
     getRegisteredUsers, getUserDetails, getAttendanceDetails, editEvent,
     deleteSchedule, editSchedule, createSchedule, removeRegisteredUser,
     getQR, createAttendanceDetails, hasAttended} from "./database.js";

/**
 * These imports are utilized for reading files
 * Converting images to blobs
 * and reconverting blobs to images
 * that will be displayed to the client
 */
import multer from "multer";
import path from "path";
import fs from "fs";

/**
 * This import handles the qr code
 * attendance system
 */
import qr from "qrcode";

/**
 * Stores the event posters/images in local file system
 * Note that the images are only stored locally to convert
 * them into base64 as a blob format, and will be saved accordingly
 * to the database. As such, even if the photos saved locally are deleted,
 * the photos can still be accessed by users
 */
const storageOptions = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, "./eposters")
    },
    filename: function (req, file, cb) {
        //console.log(file); //file -> variable containing the actual file
        cb(null, Date.now() + path.extname(file.originalname)) //access original name of file + date it got uploaded
    }  
});
const fileUpload = multer({storage: storageOptions}); 

/**
 * Initialized the application via express
 */
const app = express();

// Listening port
const port = 3000

/**
 * this app utilizes json APIs
 * Any json body will be accepted and passed into req.body objects
 */
app.use(express.json());

/** 
 *  Used to dynamically alter an html file so 
 *  as not to generate multiple ones 
 * 
 *  templates with EJS
 *  Helps in utilizing .render() methods to dynamically render files in HTTP responses 
 *  Note that by default files that are rendered by ejs have to exist in a folder "views"
 *  With EJS in our arsenal, we do not have to keep creating HTML files for every event/schedule 
 * */ 
app.set("view engine", "ejs");

/**
 * Any type of data can be sent in a form
 */
app.use(express.urlencoded({extended: true}));

/**
 * app get and posts
 * API routing section
 */


/**
 * The first directory.
 * Loads admin.ejs under /admin/event
 * admin.ejs shows an overview of every event present
 * The admin can then view, delete, and create new events 
 * under this directory
 */
app.get("/admin/event", async (req, res) => {
    const events = await getAllEvents();
    res.status(201).render("admin.ejs", {events});
});

/**
 * Helper async function for retrieving user details
 * @param {*} regUsers is an array of registered users in an event
 * @returns an array of details taken from each registered user
 */
async function returnUserDetails(regUsers){
    const arrUserDetails = []; // Holds the user details for every registered user

    for (const regUser of regUsers) {
        let userDetail = await getUserDetails(regUser.userID);
        arrUserDetails.push(userDetail);
        //console.log(userDetail);
    }
    
    return arrUserDetails;
}

/**
 * Helper async method for returning attendance details based on 
 * the number of registered users for a particular event schedule
 * @param {*} regUsers 
 * @returns array of attendace details
 */
async function returnAttendanceDetails(regUsers){
    const arrUserAttendanceDetails = []; // Holds the attendance details for every registered user
    for (const regUser of regUsers){
        let userAttendaceDetails = await getAttendanceDetails(regUser.schedID, regUser.userID);
        arrUserAttendanceDetails.push(userAttendaceDetails);
    }
    //console.log(arrUserAttendanceDetails);
    return arrUserAttendanceDetails;
}

/**
 * Event Panel
 * 
 *  When a user clicks on a view button in a particular event from /admin/event
 *  They get routed to /admin/event/:id 
 *  :id is a dynamic id pertaining to a particular event
 *  In this case the :id is the event the user viewed
 *  event.ejs will be rendered once a user lands on this page
 * 
 *  If no event exists an event404.ejs is displayed
 * 
 *  event.ejs shows the event details, schedules, and registered users
 *  admins/users can opt to:
 *  1) edit an event (routed to /admin/event/:id/editEvent)
 *  2) view a schedule (routed to /admin/event/:id/schedule/:schedID)
 *  3) go back ( routed to admin/event)
 *  
 */
app.get("/admin/event/:id", async (req, res) => {
    // This section takes event details
    const eventid = +req.params.id;
    const event = await getEventByID(eventid);

    if (!event){
        res.status(404).render("event404.ejs");
        return;
    }
    
    // This section takes schedule details
    const schedules = await getEventSchedules(eventid);

    //This section takes the event's registered users and their details
    const regUsers = await getRegisteredUsers(eventid); //array of event_registry table rows
    const userDetails = await returnUserDetails(regUsers); //array of user table rows
    
    //This section takes the registered user's attendance
    const attendanceDetails = await returnAttendanceDetails(regUsers); //array of attendance table rows
    
    res.status(201).render("event.ejs", {event, schedules, regUsers, userDetails, attendanceDetails});

});

app.post("/admin/event/:id/registeredUser/delete", async (req, res) => {
    const eventid = +req.params.id;
    const userID = +req.body.userID; 

    console.log(eventid);
    console.log(userID);

    try {
        await removeRegisteredUser(eventid, userID);
        console.log("The registered user has been removed from the event.");
        res.status(201).redirect(`/admin/event/${eventid}`);
    } 
    catch (error) {
        console.error('Error removing user:', error);
        // Handle error and send an appropriate response.
        res.status(500).send('Internal Server Error');
    }

});

/**
 * User gets sent to this API directory when a person "clicks" on a particular schedule under the Event Panel
 * A particular schedule and its details will be shown
 * Admins may:
 * 1) Edit a schedule via the "Edit Schedule" button *(routed to /admin/event/:id/schedule/:schedID/editSchedule)
 * 2) Delete a schedule via the "Delete Schedule" button *(routed to /admin/event/:id/schedule/:schedID/deleteSchedule)
 * 3) Go back to the previous page via the "Back" button *(routed to /admin/event/:id)
 * 
 */
app.get("/admin/event/:id/schedule/:schedID", async (req, res) => {
    const eventid = +req.params.id;
    const event = await getEventByID(eventid);
    if (!event){
        res.status(404).render("event404.ejs");
        return;
    }

    const scheduleID = +req.params.schedID;
    const [schedule] = await getScheduleDetails(scheduleID);
    if (!schedule){
        res.status(404).render("schedule404.ejs");
    }

    console.log(schedule);
    res.status(201).render("schedule.ejs", {event, schedule});
});

/**
 * After clicking the "Edit Schedule" button
 * It rendered a form with editSchedule.ejs
 * If there was a happenstance where there was no event and schedule under this;
 * 404 ejs's will be rendered respectively
 * 
 * editSchedule.ejs is a post form that essentially allows an admin to edit a schedule
 * The original data within the fields will be retained and can be modified accordingly
 * The query method for UPDATING can dynamically see which data were changed and can
 * retain the fields that weren't changed.
 */
app.get('/admin/event/:id/schedule/:schedID/editSchedule', async (req, res) => {
    const eventid = +req.params.id;
    const schedID = +req.params.schedID;

    // Get event that hosts the schedule
    const event = await getEventByID(eventid);
    if (!event){
        res.status(404).render("event404.ejs");
        return;
    }

    // Get schedule under event
    const [schedule] = await getScheduleDetails(schedID);
    if (!schedule){
        res.status(404).render("schedule404.ejs");
    }

    res.status(201).render("editSchedule.ejs", {event, schedule});
});

/**
 * This API route is where the user/admin must 
 * be taken once they click the "create event" button
 * found in the /admin/event API route
 * 
 * createEventForm.ejs is a post form that allows an admin to create a new event
 * createEventForm.ejs has a POST request that routes to /admin/event
 */
app.get("/admin/createEvent", async (req, res) => {
    res.render("createEventForm.ejs");
});

/**
 * API Routing for about us page.
 */
app.get("/admin/aboutus", async (req, res) => {
    res.render("aboutus.ejs");
});

/**
 * API Routing for SCAN QR page. 
 */
app.get("/admin/scanQR", async (req, res) => {
    const toScanSchedule = req.query.scheduleID;
    console.log("SCHEDULE TO SCAN: " + toScanSchedule);
    res.render("ScanQR.ejs", { toScanSchedule });
});

app.get("/admin/scanQR/attendance", async (req, res) => {
    //const schedules = req.query.schedules;
    const userID = req.query.userID;
    const eventID = req.query.eventID;
    const scheduleID = req.query.scheduleID;
    const toScanSchedule = req.query.toScanSchedule;
    
    console.log("userID: " + userID);
    console.log("Passed eventID to the form: " + eventID);
    console.log("Passed scheduleID to the form: " + scheduleID);
    console.log("Schedule Currently Being Scanned: " + toScanSchedule);

    const eventDetails = await getEventByID(eventID);
    //const event_name =  eventDetails.event_name;
    //console.log("event details: " + event_name + " " + eventID);

    // Render the validateRegistration.ejs with schedules and userDetails
    if(scheduleID == toScanSchedule){
        try{
            res.render("validateRegistration.ejs", { scheduleID, userID, eventID });
        }
        catch (error){
            console.error("There was an error rendering validate registration ejs: " + error);
        }
    }
    else {
        console.log("USER HAS NOT REGISTERED IN THIS EVENT");
        res.redirect(`/admin/event/${eventID}/schedule/${scheduleID}`);
        // may pop-up sana HAHAHA
    }
    
    
});

/**
 * Post request tied to validateRegistration.ejs
 */
app.post("/admin/scanQR/attendance/submit-attendance", async (req, res) => {
    // get data needed
    const userID = req.body.userID;
    console.log("User ID from post form submitting-attendance: " + userID);
    const eventID = req.body.eventID;
    console.log("Event ID from post form submitting-attendance: " + eventID);
    const scheduleID =  req.body.scheduleID;
    console.log("Schedule ID from post form submitting-attendance: " + scheduleID);

    // update attendance with qr varchar in database
    const userAttended = await hasAttended(scheduleID, userID);
    if (userAttended){
        console.log("USER HAS ALREADY REGISTERED");
        res.redirect(`/admin/event/${eventID}/schedule/${scheduleID}`)
        // may pop up sana
    }
    else {
        await createAttendanceDetails(userID, scheduleID, eventID);
        res.redirect(`/admin/event/${eventID}/schedule/${scheduleID}`)
    }
    
});

/**
 * This API route is where the user/admin must
 * be taken once they click the "edit event" button rendered from event.ejs
 * on the /admin/event/:id/editEvent
 * 
 * editEvent.ejs is a post form that retains the previous data
 * from the database and the admin can change the field data
 * accordingly. The query responsible for updating can dynamically
 * identify which data fields were changed. Changes made will be pushed to
 * the database while the retaining and unchanged data is saved as is.
 * 
 */
app.get("/admin/event/:id/editEvent", async (req, res) => {
    const eventid = +req.params.id;
    const event = await getEventByID(eventid);
    res.status(201).render("editEvent.ejs", {event});
});

/**
 * Admins get routed to this directory after clicking the
 * "Add Schedule" button.
 * createScheduleForm.ejs will be rendered
 * It is a post form that creates a schedule for /admin/event/:id/createSchedule
 */
app.get("/admin/event/:id/createSchedule", async (req, res) => {
    const eventid = +req.params.id;
    const event = await getEventByID(eventid);
    res.status(201).render("createScheduleForm.ejs", {event});
})

/**
 * The post request for creating an event, images are handled accordingly
 * through a base64 conversion in order to safely store in the database
 * as a blob format.
 * 
 * The data taken from the admin's form response will be allocated to the
 * createEvent() query to create said event and save it into the database. 
 */
app.post("/admin/event", fileUpload.single("poster"), async (req, res) => {
    const event_name = req.body.event_name;
    const date = req.body.date;
    const description = req.body.description;
    const fee = +req.body.fee;
    const organization = req.body.organization;
    const status = req.body.status;

    //Uploaded poster
    //const poster = req.file;

    // Read the poster as a buffer
    const buffer = fs.readFileSync(req.file.path);

    // Convert the buffer to base64 encoding
    const base64Poster = buffer.toString("base64");
    
    const newEvent = await createEvent(event_name, description, fee, organization, date, status, base64Poster);
    //console.log(newEvent);
    res.redirect("/admin/event");
});

/**
 * The post request that takes data from editEventForm.ejs and
 * Dynamically changes or updates the data in the event table
 * based on what the admin changed.
 */
app.post("/admin/event/:id/editEvent", fileUpload.single("poster"), async (req, res) =>{
    const eventid = +req.params.id;
    const updatedEventData = req.body;

    // Check if a new poster file was uploaded
    let base64Poster = null;
    if (req.file) {
        // Read the poster as a buffer
        const buffer = fs.readFileSync(req.file.path);
        // Convert the buffer to base64 encoding
        base64Poster = buffer.toString("base64");
    }

     // Add the base64 poster to the updatedEventData if it exists instead
     if (base64Poster) {
        updatedEventData.poster = base64Poster;
    }

    try {
        await editEvent(eventid, updatedEventData);
        console.log("Event updated!");
        res.redirect(`/admin/event/${eventid}`);
    } 
    catch (error) {
        console.error('Error updating event:', error);
        res.status(500).send('Internal Server Error');
    }

});

/**
 * Post request for creating a schedule
 * Takes data from the form "createScheduleForm"
 * and parses the data from the request body and
 * distributed the endpoints accordingly to the appropriate variables
 * Each variable is passed into the createSchedule() query method
 */
app.post("/admin/event/:id/createSchedule", async (req, res) => {
    const eventid = +req.params.id;
    const description = req.body.description;
    const venue = req.body.venue;
    const date = req.body.date;
    const status = req.body.status;

    try {
        const newSched = await createSchedule(eventid, description, venue, date, status);
        //console.log("Schedule Created: " + JSON.stringify(newSched));
        res.redirect(`/admin/event/${eventid}`);
    } 
    catch (error) {
        console.error('Error creating schedule:', error);
        res.status(500).send('Internal Server Error');
    }
});

/**
 * After clicking the "Edit Schedule" button this is the post request that
 * handles the editing of a schedule.
 */
app.post("/admin/event/:id/schedule/:schedID/editSchedule", async (req, res) => {
    const eventid = +req.params.id;
    const schedID = +req.params.schedID;
    const updatedSchedData = req.body;

    try {
        await editSchedule(schedID, updatedSchedData);
        console.log("Schedule updated!");
        res.redirect(`/admin/event/${eventid}/schedule/${schedID}`);
    } 
    catch (error) {
        console.error('Error updating schedule:', error);
        res.status(500).send('Internal Server Error');
    }
});

/**
 * After clicking the "Delete event" button,
 * handles the deletion and removal of a schedule in the database
 */
app.post("/admin/event/:id/schedule/:schedID/deleteSchedule", async (req, res) => {
    const eventid = +req.params.id;
    const schedID = +req.params.schedID;
    console.log("scheduleID to be deleted: " + schedID);
    const deletedSchedule = await deleteSchedule(schedID);
    console.log("Your event has been deleted!");
    res.redirect(`/admin/event/${eventid}`);
});

/**
 * Deletes a particular event
 * A button is assigned to each event 
 * Clicking that button will erase that row from
 * the database (a row from an event table given an eventid)
 * 
 * */
app.post("/admin/event/:id/delete", async (req, res) => {
    const eventid = +req.params.id;
    console.log("eventid to be deleted: " + eventid);
    const deletedEvent = await deleteEventWithID(eventid);
    console.log("Your event has been deleted!");
    res.redirect("/admin/event");
})

// The CSS or any file is used by default in public folder
app.use(express.static("public"));

app.use((err, req, res, next) => {
  console.error(err.stack);
  res.status(500).send("Error somewhere lmao");
});

app.listen(port, () => {
  console.log(`Event Handler listening on port ${port}`)
});