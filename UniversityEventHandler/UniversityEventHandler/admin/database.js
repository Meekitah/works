/**
 * Code Author of database.js: Samson, Mik Iris G.
*/

import mysql from "mysql2";

/**
 * This is the connection to the MySQL Server
 * change the host and database name accordingly to
 * the supposed machine it must connect to and SQL server
 * database name
 */
const pool = mysql.createPool({
    host: "127.0.0.1",
    user: 'root',
    password: '',
    database: 'gigagingers'
}).promise();

export async function getAllEvents() {
    try {
        const res = await pool.query("SELECT * FROM event");
        const events = res[0];
        //console.log(events);
        return events;
    } 
    catch (error) {
        console.error("Error fetching data:", error.message);
    }
}

export async function getEventByID(id){
    const [rows] = await pool.query(`
    SELECT *
    FROM event
    WHERE eventid = ?`, [id]);

    const event = rows[0];
    //console.log(event)
    return event;
}

export async function getEventByName(eventName){
    const [rows] = await pool.query(`
    SELECT *
    FROM event
    WHERE event_name = ?`, [eventName]);

    const event = rows[0];
    //console.log(event)
    return event;
}

/**
 * 
 * @param {*} givenDate  is a date in the format (YYYY-MM-DD)
 * @returns a boolean that checks if the event date will be held 
 * in a possible date (not in the past)
 */
function isEventDateValid(givenDate){
    const currentDate = new Date();
    const eventDate = new Date(givenDate);
    return eventDate > currentDate;
}

/**
 * Formats appropriate Zeroes into hour format in displaying time
 * Is utilized in addZero() method
 */
function addZero(number){
    return number < 10 ? `0${number}`:number;
}

/**
 * Utilized in date_created field in database
 * Follows the format: YYYY-MM-DD HH:MM:SS
 * @returns The current date and time
 */
function getCurrentTime(){

    const current = new Date();
    const month = current.getMonth() + 1;
    const year = current.getFullYear();
    const date = current.getDate();
    const dateRightNow = `${year}-${month}-${date}`;

    const hours = addZero(current.getHours());
    const minutes = addZero(current.getMinutes());
    const seconds = addZero(current.getSeconds());
    const timeRightNow = `${hours}:${minutes}:${seconds}`;

    const dateTimeCurrent = `${dateRightNow} ${timeRightNow}`;
    return dateTimeCurrent;
}

/**
 * This method creates an event and pushes it to the database
 * Current time-stamp is auto generated!
 * 
 * 
 * This is characterized by the name of the event:
 * @param {*} event_name is a varchar of 50 and cannot be null
 * This is the provided description about the event
 * @param {*} description is a varchar of 100 and is null by default (nullable)
 * Event fee is how much a participant would be charged to
 * enter or participate in the event:
 * @param {*} fee double value that can be 
 * The organization that hosted the event:
 * @param {*} organization is a varchar of 45 and can be null by default
 * The date when the event would start and is parsed to be 
 * readable by MySQL:
 * @param {*} date (YYYY-MM-DD)
 * The current status of the event:
 * @param {*} status (coming soon, finished, ongoing, postponed, canceled)
 * @param {*} posterBlob A blob format of an event poster
 * @returns a json object of the newly created event
 */
export async function createEvent(event_name, description, fee, organization, date, status, posterBlob){
    try {

        // Create the event if the event date is valid
        if (isEventDateValid(date)) {
            
            // Makes sure the date parameter is in a MySQL-compatible string
            const formattedDate = new Date(date).toISOString().slice(0, 10);
            const currentDateTime = getCurrentTime();

            const [queryRes] = await pool.query(`
                INSERT INTO event (event_name, description, fee, organization, date, date_created, status, poster) 
                VALUES (?, ?, ?, ?, ?, ?, ?, ?)`, 
                [event_name, description, fee, organization, formattedDate, currentDateTime, status, posterBlob]);

            // Returns a json object of the created event
            const id = queryRes.insertId;       
            return getEventByID(id);
        } 
        else {
            console.log("Event date is in the past. Cannot create the event.");
        }     
    }
    catch(error)
    {
        console.error("There was an error in either creating the " +
        "event or showing the newly created event:", error.message)
    }  
}

/**
 * Given an id, delete that event the id is referring to (row) from events database
 * @param id 
 */
export async function deleteEventWithID(id){
    try{
        const [deletedEvent] = await pool.execute(`DELETE FROM event WHERE eventid= ? `, [id]);
        if (deletedEvent.affectedRows > 0) {
            console.log(`Event with eventid ${id} deleted successfully.`);
        } 
        else {
            console.log(`Event with eventid ${id} not found.`);
        }
    }
    catch(error){
        console.error("There was an error in deleting the " +
        "event: ", error.message)
    }  
}

/**
 * Given a matching eventName, delete that event (row) from the database
 * @param {*} eventName as name of the event that will be deleted
 */
export async function deleteEventWithName(eventName){
    try{
        const [deletedEvent] = await pool.execute(`DELETE FROM event WHERE event_name= ? `, [eventName]);
        if (deletedEvent.affectedRows > 0) {
            console.log(`Event called: ${eventName} deleted successfully.`);
        } 
        else {
            console.log(`Event called: ${eventName} not found.`);
        }
    }
    catch(error){
        console.error("There was an error in deleting the " +
        "event: ", error.message)
    }
}

/**
 * The intention of this method is to be able to
 * delete completed events. As of now a custom status "failed"
 * will be used to delete a multitude of rows with said status for testing.
 * The reason why it isn't parameterized is that it is intended only 
 * for "completed" or "finished" events where the admin wished to delete
 * events that are done by batch when not needed
 */
export async function deleteCompletedEvents(){
    try{
        const statusToRemove = "failed";
        const [deletedEvent] = await pool.execute(`
        DELETE FROM event WHERE status = ?`, [statusToRemove]);
        //console.log(deletedEvent);
        if (deletedEvent.affectedRows > 0) {
            console.log(`Event with statuses: "${statusToRemove}" deleted successfully.`);
        } 
        else {
            console.log(`Events have no such status: ${statusToRemove}.`);
        }
    }
    catch (error){
        console.error("There was an error in deleting the " +
        "event: ", error.message);
    }
}

/**
 * Each event can have multiple schedules
 * getEventSchedules(id) takes all of the schedules associated within an event
 * as JSON objects
 * @param {*} eventID is the eventid and the primary key of the event table
 * @returns 
 * {
 * scheduleID: schedule_id_value,
 * venue:  'venue_value',
 * date: date_value,
 * status: 'status_value'
 * }
 */
export async function getEventSchedules(eventID){
    try{
        const [schedules] = await pool.execute(`
        SELECT scheduleID, venue, date, status
        FROM schedule
        WHERE eventid = ?`, [eventID]);

        if(schedules.length == 0){
            console.log("There are no associated schedules for this event.");
        }

        //console.log(schedules);
        return schedules;
    }
    catch(error){
        console.error("There was trouble looking for the event's schedules: ", error.message);
    }
}

/**
 * 
 * Events can have 0 or more schedules.
 * This method gets the details of a particular schedule through a scheduleID
 * 
 * @param {*} schedID primary key that holds a specific schedule of an event
 * @returns a json object of a schedule:
 * example:
 * {
    scheduleID: 8000,
    eventID: 9011,
    description: 'Introductory Session: Understanding Leadership Principles',
    venue: 'SLU Main Auditorium',
    date: 2023-12-08T01:00:00.000Z,
    status: 'Pending'
  }
 */
export async function getScheduleDetails(schedID){
    try{
        const [schedule] = await pool.execute(`
        SELECT * FROM schedule
        WHERE scheduleID = ?`, [schedID])

        //console.log(schedule);
        return schedule;
    }
    catch(error) {
        console.error("There was an error fetching the schedule for that event: ", error.message);
    }
}

/**
 * A schedule can be characterized as a "sub-event" and a given time and place where
 * that sub-event will be held.
 * 
 * @param {*} eventID the unique event that this new schedule is under
 * @param {*} description a description about what this schedule is for
 * @param {*} venue where it'll be held
 * @param {*} date when the event schedule will be held
 * @param {*} status for the event (Pending, In Progress, Finished, Canceled, Postponed)
 * @returns the newly created schedule
 */
export async function createSchedule(eventID, description, venue, date, status){
    try {

        // Create the schedule and check if the date is valid
        if (isEventDateValid(date)) {
            
            // Makes sure the date parameter is in a MySQL-compatible string
            const formattedDate = new Date(date).toISOString().slice(0, 19).replace('T', ' ');
            //const currentDateTime = getCurrentTime();

            const [schedule] = await pool.query(`
                INSERT INTO schedule (eventID, description, venue, date, status) 
                VALUES (?, ?, ?, ?, ?)`, 
                [eventID, description, venue, formattedDate, status]);
            
                // Returns a json object of the created schedule
            const id = schedule.insertId;       
            return getScheduleDetails(id);
        } 
        else {
            console.log("Schedule date is in the past. Cannot create the schedule.");
        }     
    }
    catch(error)
    {
        console.error("There was an error in either creating the " +
        "schedule or showing the newly created schedule:", error.message)
    }  
}

/**
 * This method allows an admin to edit a schedule and push it
 * to the database.
 * The current data for the schedule that is being edited will be fetched
 * The update query will be dynamically updated based on what
 * was changed or edited in the post request (schedule form)
 * An update query will then be made
 * 
 * @param {*} schedID as the unique schedule to be edited
 * @param {*} updatedSchedData holds the body of the data to be updated
 * @returns 
 */
export async function editSchedule(schedID, updatedSchedData){
    // Fetch the current data from the database
    try {
        const [currentSchedData] = await pool.execute('SELECT * FROM schedule WHERE scheduleID = ?', [schedID]);
        const currentEvent = currentSchedData[0];

        const updateFields = [];
        const updateValues = [];

        for (const [key, value] of Object.entries(updatedSchedData)) {
            //console.log(`Key: ${key}, Current Value: ${currentEvent[key]}, New Value: ${value}`);
            
            if (currentEvent[key] !== value) {
                console.log("Value has changed. Processing...");
                
                updateFields.push(`${key} = ?`);
        
                if (key === 'date') {
                    const formattedDate = new Date(value).toISOString().slice(0, 19).replace('T', ' ');
                    console.log(`Formatted Date: ${formattedDate}`);
                    updateValues.push(formattedDate);
                } 
                else {
                    updateValues.push(value);
                }
            } 
            else {
                console.log("Value has not changed. Skipping...");
            }
        }

        if (updateFields.length === 0) {
            // No changes, nothing to update
            console.log("No changes to update.");
            return;
        }

        const updateQuery = `UPDATE schedule SET ${updateFields.join(', ')} WHERE scheduleID = ?`;
        updateValues.push(schedID);
        console.log("Update Query:", updateQuery);
        console.log("Update Values:", updateValues);

        await pool.execute(updateQuery, updateValues);
    } 
    catch (error) {
        console.error("Error editing the schedule details:", error.message);
    }
}

/**
 * 
 * Deletes a schedule row based on a unique scheduleID
 * @param {*} scheduleID unique schedule to be deleted
 */
export async function deleteSchedule(scheduleID){
    try{
        const [deletedSchedule] = await pool.execute(`DELETE FROM schedule WHERE scheduleID= ? `, [scheduleID]);

        if (deletedSchedule.affectedRows > 0) {
            console.log(`Event with eventid ${scheduleID} deleted successfully.`);
        } 
        else {
            console.log(`Event with eventid ${scheduleID} not found.`);
        }
    }
    catch(error){
        console.error("There was an error in deleting the " +
        "event: ", error.message)
    }  
}

/**
 * Each event has registered users and this method queries the
 * participants in an event saved in the database!
 * 
 * @param {*} eventID pertaining to a unique event where users are registered
 * @returns an array or list of participants toward event as JSON Objects
 */
export async function getRegisteredUsers(eventID){
    try{
        const [users] = await pool.execute(`
        SELECT * FROM event_registry
        WHERE eventID = ?
        `, [eventID]);

        /**
         * Users from this point on are returned as 
         * an array of (JSON Object format) participants in an event
         * A unique event is referred to by the eventid
        */
        //console.log(users);
        if (users.length == 0) {
            console.log("There are no registered users yet for this event.");
        }
        //console.log(users);
        return users;
    }
    catch(error) {
        console.error("There was an error fetching the participants that event: ", error.message);
    }
}

export async function createAttendanceDetails(userID, scheduleID, eventID){
    try {
        const currentDateTime = getCurrentTime();

        const [queryRes] = await pool.query(`
            INSERT INTO attendance (userID, scheduleID, eventID, time_in)
            VALUES (?, ?, ?, ?)`,
            [userID, scheduleID, eventID, currentDateTime]);

        // Check if the insert was successful
        if (queryRes.affectedRows > 0) {
            console.log("Attendance created successfully.");
        } 
        else {
            console.log("Failed to create attendance.");
        }
    } 
    catch (error) {
        console.error("Error creating attendance:", error.message);
    }
}

/**
 * A method that queries a user's details
 * @param {*} userID a particular user as a primary key in the user table
 * @returns a JSON object containing a user's primary details
 */
export async function getUserDetails(userID){
    try{
        const [userDetails] = await pool.execute(`
        SELECT * FROM user
        WHERE userID = ?
        `, [userID]);

        /* Only one JSON object of a user is returned
         * so we're only taking the first element from the array
         */
        //console.log(userDetails[0]);
        if (userDetails.length == 0){
            console.log("There appears to be no such user fetched from the database");
        }
        return userDetails[0];
    }
    catch(error) {
        console.error("There was an error fetching the details of the user: ", error.message);
    }
}

/**
 * Get attendance details of a user that has registered for an event
 * It uses the unique QR value of a user (from event_registry table) as a foreign key to
 * query that user's details in the attendance (scheduleID and time_in)
 * However for now the time_in will only be extracted, so the endpoint of `time_in`
 * shall be the return value of this method
 * 
 * Registered users may opt to not attend, for that,
 * a custom json object for not attending is returned
 * Considering that schedID can't be null, schedID is
 * passed as a parameter
 * @param {*} attendance_qr and schedID
 */
export async function getAttendanceDetails(scheduleID, userID){
    try{
        const [attendance] = await pool.execute(`
        SELECT * FROM attendance
        WHERE scheduleID = ? AND userID = ?
        `, [scheduleID, userID]);

        /**
         * Attendance is initially returned as an array 
         * considering that we're only taking 
         * A unique event is referred to by the eventid
        */
        //console.log(attendance[0]);

        if(attendance.length == 0){
            console.log("There appears to be no attendance details from the generated QR code!");
            const attendFalse = {
                qr: "did not attend",
                scheduleID: 1000,
                time_in: null,
                userID: 100
            }
            return attendFalse;
        }
        else {
            return attendance[0];
        }
        
    }
    catch(error) {
        console.error("There was an error fetching the attendance details of a person for that schedule: ", error.message);
    }
}

export async function hasAttended(scheduleID, userID){
    try{
        const [attendance] = await pool.execute(`
        SELECT * FROM attendance
        WHERE scheduleID = ? AND userID = ?
        `, [scheduleID, userID]);

        /**
         * Attendance is initially returned as an array 
         * considering that we're only taking 
         * A unique event is referred to by the eventid
        */
        //console.log(attendance[0]);

        if(attendance.length == 0){
            console.log("This user hasn't attended yet");           
            return false;
        }
        else {
            return true;
        }
        
    }
    catch(error) {
        console.error("There was an error fetching the attendance details of a person for that schedule: ", error.message);
    }
}



/**
 * Gets the blob qr code from the event_registry method
 * @param {} userID as the user who registered for a certain schedule in an event
 * @param {*} eventID as the particular event the user registered to
 */
export async function getQR(userID, eventID) {
    const query = 'SELECT QR FROM event_registry WHERE userID = ? AND eventID = ?';

    try {
        const [results] = await pool.query(query, [userID, eventID]);

        if (results.length > 0) {
            // Assuming there is only one matching record
            const qrBlob = results[0].QR;
            //console.log(results);
            return qrBlob;
        } 
        else {
            return null;
        }
    } 
    catch (error) {
        console.error('Error retrieving QR code:', error);
        throw error;
    }
}

/**
 * This method allows an admin to edit an event and push it
 * to the database.
 * The current data for the event that is being edited will be fetched
 * The update query will be dynamically updated based on what
 * was changed or edited in the post request (edit form)
 * An update query will then be made
 * 
 * @param {*} eventid as the primary key of an event
 * @param {*} updatedEventData as a body from a post request;
 * updatedEventData contains the changes made
 * @returns 
 */
export async function editEvent(eventid, updatedEventData){

    try {
        // Fetch the current data from the database
        const [currentEventData] = await pool.execute('SELECT * FROM event WHERE eventid = ?', [eventid]);
        const currentEvent = currentEventData[0];

        /** 
         * This section checks if a new poster was uploaded during editing
         * If no new poster was uplloaded, retain the same poster!
         * */ 
        const newPoster = updatedEventData.poster;
        if (!newPoster) {
            // If no new poster is uploaded, use the current poster value
            updatedEventData.poster = currentEvent.poster;
        }

        // Build the update query dynamically based on changes in the form
        const updateFields = [];
        const updateValues = [];

        for (const [key, value] of Object.entries(updatedEventData)) {
            // Check if the value has changed
            if (currentEvent[key] !== value) {
                updateFields.push(`${key} = ?`);
                updateValues.push(value);
            }
        }

        if (updateFields.length === 0) {
            // No changes, nothing to update
            return;
        }

        // Update the database
        const updateQuery = `UPDATE event SET ${updateFields.join(', ')} WHERE eventid = ?`;
        updateValues.push(eventid);
        await pool.execute(updateQuery, updateValues);
    }

    catch(error){
        console.error("There was an error editing the event details: ", error.message);
    }
}

export async function removeRegisteredUser(eventID, userID){
    try {
        const query = `DELETE FROM event_registry 
        WHERE eventID= ? AND userID= ?`;

        const [results, fields] = await pool.execute(query, [eventID, userID]);
        
        if (results.affectedRows > 0) {
            console.log(`Row with userID ${userID} and eventID ${eventID} deleted successfully.`);
        } 
        else {
            console.log(`No rows were deleted for userID ${userID} and eventID ${eventID}.`);
        }

    }
    catch(error){
        console.error("There was an error in deleting the " +
        "registered user: ", error.message)
    }
}

/**
 * 1) TESTING GETTING EVENTS
 * Tester methods are found below
 * Uncomment them to use them
 * Additional instructions for testing will be given in following
 * comment lines
 * To run them, go to terminal and type "node database.js"
 */

//getAllEvents();
//getEventByID(9001);
//getEventByName("Tech Talks: Innovation and the Future");

/**
 * 2) TESTING CREATION AND DELETING EVENTS
 * The following create events are sample events that will be created
 * The three other delete methods will be used to remove the created
 * events via an id and an explicit name. 
 * (Note that the id of the events may vary depending on how many times 
 * it had been tested, as of now the Ken Carson Concert has the 9012 id)
 * 
 * The last two created events will be for deleting same statuses via deleteCompletedEvents();
 */

//createEvent("Ken Carson Concert", "Ken Carson visits SAMCIS and exposes their new rage genre", 500, "Opium Label", "2023-12-18", "coming soon");
//createEvent("Ka-Bravo E-utan", "Participate in the SAMCIS online games and claim the championship", 100, "Saint Louis University", "2023-12-17", "coming soon");
//createEvent("Ka-Bravo E-utan Part 2", "Participate in the SAMCIS online games and claim the championship", 100, "Saint Louis University", "2023-12-17", "failed");
//createEvent("Ampersand on Devesse", "Party time with SAMCIS Ka-Bravos", 200, "Saint Louis University", "2023-12-20", "failed");
//deleteEventWithID(9012);
//deleteEventWithName("Ka-Bravo E-utan");
//deleteCompletedEvents();

/**
 * 3) TESTING QUERIES FOR AN EVENT'S GIVEN SCHEDULES
 * The following tests search for an event's schedule based on the eventid
 * getting Event schedules will yield some schedules for eventid 9001
 * while event id 9002 will yield none because it has no schedules set (as of testing)
 * 
 * getScheduleDetails(schedID) gets the details of an event's particular schedule (via a unique scheduleID)
 * createSchedule below creates a schedule given a particular eventID, description, venue, date to be held, and status!
 * deleteSchedule(8013) deletes the previously created schedule from createSchedule() which made
 * a new schedule with scheduleID 8013. 
 */

//getEventSchedules(9001);
//getEventSchedules(9002);
//getScheduleDetails(8000);
//const sched1 = await createSchedule(9022, "Green Room Performance", "SLU MARYHEIGHTS AVR", "2023-12-20", "Pending");
//console.log("Newly created schedule: " + sched1);
//deleteSchedule(8013);

/**
 * 4) TESTING QUERIES RELATED TO AN EVENT'S PARTICIPANTS
 * The following tests, starting with `getRegisteredUsers()` returns the registered users in a particular event schedule.
 * 
 * The details of each user is returned as well with getUserDetails()
 * In this instance, getUserDetails() method takes the details of a user with userID `102`
 * 
 * A person has to be registered to attend an event. An event can have multiple schedules thus 
 * a qr code from a registered person in a particular schedule must be made
 * schedule attendance details of a person can be obtained through the qr code
 * QR code is in varchar and is passed on to the method getAttendanceDetails()
 * getQR checks for the user's (userID) qr code under a particulat event (eventid) that they've registered
 */
//getRegisteredUsers(9001);
//getUserDetails(102);
//getAttendanceDetails("10190012023-10-30", 9001);
//await getQR(106, 9001);