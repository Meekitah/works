package wordyApp;


/**
* wordyApp/Exceed10Secs.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from Wordy.idl
* Friday, July 7, 2023 7:38:10 PM SGT
*/

public final class Exceed10Secs extends org.omg.CORBA.UserException
{
  public String message = null;

  public Exceed10Secs ()
  {
    super(Exceed10SecsHelper.id());
  } // ctor

  public Exceed10Secs (String _message)
  {
    super(Exceed10SecsHelper.id());
    message = _message;
  } // ctor


  public Exceed10Secs (String $reason, String _message)
  {
    super(Exceed10SecsHelper.id() + "  " + $reason);
    message = _message;
  } // ctor

} // class Exceed10Secs
