package wordyApp.WordyPackage;


/**
* wordyApp/WordyPackage/hallOfFameHelper.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from Wordy.idl
* Friday, July 7, 2023 7:38:10 PM SGT
*/


/* Displays leading players */
abstract public class hallOfFameHelper
{
  private static String  _id = "IDL:wordyApp/Wordy/hallOfFame:1.0";

  public static void insert (org.omg.CORBA.Any a, wordyApp.LeaderBoard[] that)
  {
    org.omg.CORBA.portable.OutputStream out = a.create_output_stream ();
    a.type (type ());
    write (out, that);
    a.read_value (out.create_input_stream (), type ());
  }

  public static wordyApp.LeaderBoard[] extract (org.omg.CORBA.Any a)
  {
    return read (a.create_input_stream ());
  }

  private static org.omg.CORBA.TypeCode __typeCode = null;
  synchronized public static org.omg.CORBA.TypeCode type ()
  {
    if (__typeCode == null)
    {
      __typeCode = wordyApp.LeaderBoardHelper.type ();
      __typeCode = org.omg.CORBA.ORB.init ().create_sequence_tc (0, __typeCode);
      __typeCode = org.omg.CORBA.ORB.init ().create_alias_tc (wordyApp.WordyPackage.hallOfFameHelper.id (), "hallOfFame", __typeCode);
    }
    return __typeCode;
  }

  public static String id ()
  {
    return _id;
  }

  public static wordyApp.LeaderBoard[] read (org.omg.CORBA.portable.InputStream istream)
  {
    wordyApp.LeaderBoard value[] = null;
    int _len0 = istream.read_long ();
    value = new wordyApp.LeaderBoard[_len0];
    for (int _o1 = 0;_o1 < value.length; ++_o1)
      value[_o1] = wordyApp.LeaderBoardHelper.read (istream);
    return value;
  }

  public static void write (org.omg.CORBA.portable.OutputStream ostream, wordyApp.LeaderBoard[] value)
  {
    ostream.write_long (value.length);
    for (int _i0 = 0;_i0 < value.length; ++_i0)
      wordyApp.LeaderBoardHelper.write (ostream, value[_i0]);
  }

}
