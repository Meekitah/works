package wordyApp;


/**
* wordyApp/AlreadyLoggedIn.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from Wordy.idl
* Friday, July 7, 2023 7:38:10 PM SGT
*/

public final class AlreadyLoggedIn extends org.omg.CORBA.UserException
{
  public String message = null;

  public AlreadyLoggedIn ()
  {
    super(AlreadyLoggedInHelper.id());
  } // ctor

  public AlreadyLoggedIn (String _message)
  {
    super(AlreadyLoggedInHelper.id());
    message = _message;
  } // ctor


  public AlreadyLoggedIn (String $reason, String _message)
  {
    super(AlreadyLoggedInHelper.id() + "  " + $reason);
    message = _message;
  } // ctor

} // class AlreadyLoggedIn
