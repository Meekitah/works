package wordyApp;

/**
* wordyApp/UserNotFoundHolder.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from Wordy.idl
* Friday, July 7, 2023 7:38:10 PM SGT
*/

public final class UserNotFoundHolder implements org.omg.CORBA.portable.Streamable
{
  public wordyApp.UserNotFound value = null;

  public UserNotFoundHolder ()
  {
  }

  public UserNotFoundHolder (wordyApp.UserNotFound initialValue)
  {
    value = initialValue;
  }

  public void _read (org.omg.CORBA.portable.InputStream i)
  {
    value = wordyApp.UserNotFoundHelper.read (i);
  }

  public void _write (org.omg.CORBA.portable.OutputStream o)
  {
    wordyApp.UserNotFoundHelper.write (o, value);
  }

  public org.omg.CORBA.TypeCode _type ()
  {
    return wordyApp.UserNotFoundHelper.type ();
  }

}
