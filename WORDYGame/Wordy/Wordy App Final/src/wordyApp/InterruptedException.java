package wordyApp;


/**
* wordyApp/InterruptedException.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from Wordy.idl
* Saturday, June 10, 2023 4:40:53 AM SGT
*/

public final class InterruptedException extends org.omg.CORBA.UserException
{
  public String message = null;

  public InterruptedException ()
  {
    super(InterruptedExceptionHelper.id());
  } // ctor

  public InterruptedException (String _message)
  {
    super(InterruptedExceptionHelper.id());
    message = _message;
  } // ctor


  public InterruptedException (String $reason, String _message)
  {
    super(InterruptedExceptionHelper.id() + "  " + $reason);
    message = _message;
  } // ctor

} // class InterruptedException
