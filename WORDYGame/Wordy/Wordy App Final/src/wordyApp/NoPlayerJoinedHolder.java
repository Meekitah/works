package wordyApp;

/**
* wordyApp/NoPlayerJoinedHolder.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from Wordy.idl
* Friday, July 7, 2023 7:38:10 PM SGT
*/

public final class NoPlayerJoinedHolder implements org.omg.CORBA.portable.Streamable
{
  public wordyApp.NoPlayerJoined value = null;

  public NoPlayerJoinedHolder ()
  {
  }

  public NoPlayerJoinedHolder (wordyApp.NoPlayerJoined initialValue)
  {
    value = initialValue;
  }

  public void _read (org.omg.CORBA.portable.InputStream i)
  {
    value = wordyApp.NoPlayerJoinedHelper.read (i);
  }

  public void _write (org.omg.CORBA.portable.OutputStream o)
  {
    wordyApp.NoPlayerJoinedHelper.write (o, value);
  }

  public org.omg.CORBA.TypeCode _type ()
  {
    return wordyApp.NoPlayerJoinedHelper.type ();
  }

}
