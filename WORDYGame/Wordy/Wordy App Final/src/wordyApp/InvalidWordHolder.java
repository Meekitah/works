package wordyApp;

/**
* wordyApp/InvalidWordHolder.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from Wordy.idl
* Friday, July 7, 2023 7:38:10 PM SGT
*/

public final class InvalidWordHolder implements org.omg.CORBA.portable.Streamable
{
  public wordyApp.InvalidWord value = null;

  public InvalidWordHolder ()
  {
  }

  public InvalidWordHolder (wordyApp.InvalidWord initialValue)
  {
    value = initialValue;
  }

  public void _read (org.omg.CORBA.portable.InputStream i)
  {
    value = wordyApp.InvalidWordHelper.read (i);
  }

  public void _write (org.omg.CORBA.portable.OutputStream o)
  {
    wordyApp.InvalidWordHelper.write (o, value);
  }

  public org.omg.CORBA.TypeCode _type ()
  {
    return wordyApp.InvalidWordHelper.type ();
  }

}
