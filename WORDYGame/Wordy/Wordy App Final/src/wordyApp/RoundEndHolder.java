package wordyApp;

/**
* wordyApp/RoundEndHolder.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from Wordy.idl
* Friday, July 7, 2023 7:38:10 PM SGT
*/

public final class RoundEndHolder implements org.omg.CORBA.portable.Streamable
{
  public wordyApp.RoundEnd value = null;

  public RoundEndHolder ()
  {
  }

  public RoundEndHolder (wordyApp.RoundEnd initialValue)
  {
    value = initialValue;
  }

  public void _read (org.omg.CORBA.portable.InputStream i)
  {
    value = wordyApp.RoundEndHelper.read (i);
  }

  public void _write (org.omg.CORBA.portable.OutputStream o)
  {
    wordyApp.RoundEndHelper.write (o, value);
  }

  public org.omg.CORBA.TypeCode _type ()
  {
    return wordyApp.RoundEndHelper.type ();
  }

}
