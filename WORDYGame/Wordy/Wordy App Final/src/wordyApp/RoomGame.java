package wordyApp;


/**
* wordyApp/RoomGame.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from Wordy.idl
* Friday, July 7, 2023 7:38:10 PM SGT
*/

public final class RoomGame implements org.omg.CORBA.portable.IDLEntity
{
  public String gameRoomID = null;
  public String gameState = null;

  //pending, inGame, finishing
  public String roomRLetters = null;
  public boolean roundsWonIncr = false;
  public String winner = null;
  public wordyApp.Player players[] = null;
  public wordyApp.Word submittedWords[] = null;

  public RoomGame ()
  {
  } // ctor

  public RoomGame (String _gameRoomID, String _gameState, String _roomRLetters, boolean _roundsWonIncr, String _winner, wordyApp.Player[] _players, wordyApp.Word[] _submittedWords)
  {
    gameRoomID = _gameRoomID;
    gameState = _gameState;
    roomRLetters = _roomRLetters;
    roundsWonIncr = _roundsWonIncr;
    winner = _winner;
    players = _players;
    submittedWords = _submittedWords;
  } // ctor

} // class RoomGame
